/** Anatoa Drupal Plugin v1.0
 *  Copyright 2009 Univalence Ltd
 *  All rights reserved
 */

Drupal.behaviors.anatoa = function (context) {
	var Start =  function (){
		if(runStatus == 0 || runStatus == 2)
			runStatus = 1;
		if(profileIDs.length > 0)
			Run();
		$('#startButton').attr("disabled", true);
		$('#startbgProcbutton').attr("disabled", true);
		$('#pauseButton').attr("disabled", false);
	}

	var Pause = function (){
		if(runStatus == 1){
			runStatus = 2;
			$('#pauseButton').val('Resume');
		}
		else if(runStatus == 2){
			$('#pauseButton').val('Pause');
			runStatus = 1;
		    Run();
		}
	}

	var Stop = function (){runStatus = 0;}

	var Run = function () {
		$.ajax({
			type: 'POST',
			url: chkProfileUrl,
			dataType: 'json',
			success: function (req)
			{
		          if(req.responseText == 'Error:APIKey')
		          {
		          	$("#anatoaMsg").html(APIKeyNotConfigured);
		          	error = 1;
		          }
		          else if(req.responseText == 'Error:Connection')
		          {
		          	$("#anatoaMsg").html(ConnectionError);
		          	error = 1;
		          }
		          else if(req.responseText.substring(0,16) == 'Error:Processing')
		          {
		          	 $("#anatoaMsg").html("Error while Processing your Request.");
		          	 error = 1;

		          	 if(req.responseText.length > 16)
		          	 	$("#anatoaMsg").html(req.responseText);
		          }
		          else if(req.responseText == '')
		          {
		          	$("#anatoaMsg").html('There is no response from server.<br> Possible case: <br><b> &nbsp;&nbsp; 1) Your internet connection failed while processing request. <br> &nbsp;&nbsp; 2) Server not responding. </b>');
		          	error = 1;
		          }
		          else if( i < profileIDs.length )
			        {
					    temp = req.responseText.split(';');

					    if(isNaN(parseInt(temp[0])) || isNaN(parseInt(temp[1])))
					    {
					    	debugMsg = req.responseText;
					    	$("#anatoaMsg").html('Server error while processing your request.<br> Server Response :<b>'+debugMsg+'</b>');
					    	error = 1;
					    }
						else
						{
						    ProfilesScanned = ProfilesScanned + parseInt(temp[0]);
						    fProfile = fProfile + parseInt(temp[1]);

						    $("#fPComplete").html(ProfilesScanned);
						    $("#fP").html(fProfile);

						    percentageCompleted = Math.round(( ProfilesScanned ) * 100 / totalItems );
						    $("#divPercent").width( percentageCompleted + "%");

						    if(percentageCompleted > 12)
						      $("#percentageStatus").html(percentageCompleted + "% completed");

						    i++;
						    if(i < profileIDs.length && runStatus==1)
							    Run();
						    else if(i >= profileIDs.length )
						    {
							    $('#startButton').attr("disabled", true);
								$('#startbgProcbutton').attr("disabled", false);
								$('#pauseButton').attr("disabled", true);

							    $("#anatoaMsg").removeClass("error").addClass("messages");
							    $("#anatoaMsg").html('Selected profiles have been checked.');
						    }
						}
			        }

			        if(error)
			        {
			        	$("#anatoaMsg").removeClass("messages").addClass("error");
		          		$('#startButton').attr("disabled", true);
						$('#startbgProcbutton').attr("disabled", false);
						$('#pauseButton').attr("disabled", true);
			        	Stop();
			        	return;
			        }
	           },
			data: 'profileId='+profileIDs[i]
		  });
	}

	$('#startButton', context).click(Start);
	$('#pauseButton', context).click(Pause);
}

