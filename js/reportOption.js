/** Anatoa Drupal Plugin v1.0
 *  Copyright 2009 Univalence Ltd
 *  All rights reserved
 */

Drupal.behaviors.anatoa = function (context) {
	$('td input:checkbox', context).click(function() {
				var temp1=document.getElementsByTagName("input");
				var texttemp=document.getElementsByName("text[]");
				var temp=document.getElementsByName("myoption[]");

				var index;
				var j=0;

				var reg = new RegExp('users');

				for(var i=0;i<temp1.length;i++)
				 {
				 	if(temp1[i]==this && temp1[i].checked == true )
				 	{
				 		temp[j].disabled=false
				 	}
				 	if(temp1[i]==this && temp1[i].checked == false )
				 	{
				 		temp[j].selectedIndex=0;
				 		temp[j].disabled=true;
				 		texttemp[j].disabled=true
				 		eval("document.getElementById('div"+j+"').style.display = 'none'")
				 	}

				 	if(temp1[i].getAttribute('type')=='checkbox' && reg.test(temp1[i].getAttribute('name')))
				 	  j++;
				 }

	});
}

function resons(obj)
{
	var texttemp=document.getElementsByName("text[]");
	var temp=document.getElementsByName("myoption[]");

	for(var i=0;i<temp.length;i++)
	 {
	 	if(temp[i]==obj && obj.value == langOther )
	 	{
	 		index=i;
	 		texttemp[index].disabled=false
	 		eval("document.getElementById('div"+index+"').style.display = 'block'");
	 	}
	 	if(temp[i]==obj && obj.value != langOther )
	 	{
	 		index=i;
	 		texttemp[index].disabled=true
	 		eval("document.getElementById('div"+index+"').style.display = 'none'");
	 	}
	 }
}