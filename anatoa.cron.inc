<?php
/* Anatoa Drupal Plugin v1.0
*  Copyright 2009 Univalence Ltd
*  All rights reserved
*
* DISCLAIMER
* Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
* (whether such damage or losses were foreseen, foreseeable, known or otherwise):
* (a) loss of data;
* (b) loss of revenue or anticipated profits;
* (c) loss of business;
* (d) loss of opportunity;
* (e) loss of goodwill or injury to reputation;
* (f) losses suffered by third parties; or
* (g) any indirect, consequential, special or exemplary damages
* arising from the use of Anatoa.com or associated web services regardless of the
* form of action, including the use of this source code.
*/

/**
 * Shutdown function executed at cron time.
 */
function anatoa_cron_shutdown()
{

	$running = variable_get('anatoa_cronRunning',0);
	$numberOfTimesFailed = variable_get('anatoa_numberOfTimesFailed',0);

	if($running == 1 && $numberOfTimesFailed < 100)
	{
		watchdog('cron', 'Other instance of Anatoa cron script is still running......');
		variable_set('anatoa_numberOfTimesFailed', $numberOfTimesFailed+1);
		return ;
	}
	else
	{
		watchdog('cron', 'Anatoa cron script started at %time', array('%time' => format_date(time(), 'custom', 'H:i:s')));
		variable_set('anatoa_cronRunning',1);
		variable_set('anatoa_numberOfTimesFailed',0);
	}


	$fp = 0;
	$fi = 0;
	$fm = 0;
	$exitOuterForLoop = 0;
	$errorConnectionThreshHold = 0;		//cron job will exit if Connection Error occurs more then {$errorConnectionThreshHold} times
	$errorResponseThreshHold = 10;
	$connectionFailure = 0;
	$errorResponse = 0;
	$ProfileUsageCompleted = 0;
	$MessageUsageCompleted = 0;
	$ImageUsageCompleted = 0;
	$connectionError = 0;

	$arrReportedProfileIDs = array();

	$objWebService = new anatoaWebService();
	$soapclient = new anatoaSoapClient();
	$anatoaConfig = anatoaConfiguration::getInstance();
	$objDBModel = new dbModel();

	$installationDate = variable_get('anatoa_installationDate',0);

	if(!$soapclient->init())
	{
		watchdog('cron', 'Unable to initialize Anatoa SOAP web service.....');
		variable_set('anatoa_cronRunning',0);
		return ;
	}

	$resultSet = $resultSet = $objDBModel->getAPIConfig();
	$APIKey = $resultSet['APIKey'];
	$rejection_threshold = $resultSet['Rejection_Threshold'];
	$action = $resultSet['Action'];
	$full_email = $resultSet['Check_full_email'];
	$keyCheck = $resultSet['keyChecked'];

	if(empty($APIKey) || $keyCheck != 'Valid')
	{
		watchdog('cron', '( Anatoa Cron Script -> checkAPIKey ) : Incorrect APIKey, Please configure your API Key.');
		variable_set('anatoa_cronRunning',0);
		return ;
	}


	$arrMembers = $objDBModel->getMemberListForUpdating();
	$antoaCurrentProcess = '( Anatoa Cron Script -> checkProfile )';

	foreach ($arrMembers as $member)
	{
		$query ='INSERT IGNORE INTO '.$anatoaConfig->MasterTables['Profile'].' SET ProfileID = '.$member[$anatoaConfig->ProfileTable['Drupal']['priKey']].', TableName = \''.$anatoaConfig->ProfileTable['Drupal']['tblName']."'";
		db_query($query);

		////////////////////////////////////////   Call WEBSERVICE VIA SOAP
		if($full_email == 'Yes')
		{
			$emailAddressHash = '';
			$emailAddress = $member['mail'];
		}
		else
		{
			$emailAddressHash = md5($member['mail']);
			$emailAddress = '';
		}


		$profileIPAddress = anatoaUtility::validateIpAddress($member['IPAddress']) ? $member['IPAddress'] : '';
		$profileProxyIPAddress = anatoaUtility::validateIpAddress($member['ProxyIP']) ? $member['ProxyIP'] : '';

		$profileParams = array(	'UserName'=>$member['name'],
								'EmailHash'=>$emailAddressHash,
								'Email'=>$emailAddress,
								'IPAddress'=>$profileIPAddress,
								'ProxyIP'=>$profileProxyIPAddress );

		$fields = $objDBModel->getFieldList();

		$firstNameFieldID = variable_get('anatoa_FirstName',0);
		$lastNameFieldID = variable_get('anatoa_LastName',0);
		$countryFieldID = variable_get('anatoa_Country',0);

		$customFieldsData = $objDBModel->getCustomProfileFieldData($member['uid'],$firstNameFieldID.','.$lastNameFieldID.','.$countryFieldID);
		$customFieldsData[0] = '';

		if(!empty($customFieldsData[$firstNameFieldID]))
		$profileParams['FirstName'] = $customFieldsData[$firstNameFieldID];

		if(!empty($customFieldsData[$lastNameFieldID]))
		$profileParams['LastName'] = $customFieldsData[$lastNameFieldID];

		if( $anatoaConfig->reqMod['anatoa_addresses_user'] )
		{
			$userCountry = $objDBModel->getAddressCountry($member[$anatoaConfig->ProfileTable['Drupal']['priKey']]);
		    if(!empty($userCountry))
				$profileParams['Country'] = strtoupper($userCountry);
		}
		elseif(!empty($customFieldsData[$countryFieldID]))
			$profileParams['Country'] = $customFieldsData[$countryFieldID];

		if( !$soapclient->callMethod( $APIKey, 'checkProfile', 'Profiles', array( 'Profile'=>$profileParams ), $returnValues ) )
		{
			watchdog('cron', " $antoaCurrentProcess : [ Error Connection ] Unable to connect. ");
			$connectionError = 1;
			variable_set('anatoa_cronConnectionError',1);
			variable_set('anatoa_cronRunning',0);
			return ;
		}

		$return = $returnValues[0];

		$fraudulent = 0;
		$profileParams['APIKey'] = $APIKey;

		if(!is_numeric($return))
		{
			watchdog('cron', " $antoaCurrentProcess : [ Error Response : $return ] Error in processing you request. ");

			if($return == 'USAGE_COMPLETE')
			{
				$ProfileUsageCompleted = 1;
				break;
			}
			if($errorResponse < $errorResponseThreshHold)
			{
				$errorResponse++;

				$query ='UPDATE '.$anatoaConfig->MasterTables['Profile']." SET CheckStatus = 'Failed Check',checkReasons = '$return' WHERE TableName = 'users' AND ProfileID = {$member['uid']};";
				db_query($query);

				$objDBModel->enterLog('Check Profile','Auto Check : Cron Job',$member['uid'],$member['uid'],$anatoaConfig->ProfileTable['Drupal']['tblName'],addslashes(serialize($profileParams)),$return);

				continue;
			}
			else
			{
				watchdog('cron', " $antoaCurrentProcess : [ Error Response : $return ]. ");
				variable_set('anatoa_cronRunning',0);
				return ;

			}
		}
		elseif($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
		{
			$fp++;
			$fraudulent = 1;
		}

		if($return > 6.0)
		{
			$contents = '';
			$contents = $objWebService->reportProfile($member['uid'],'Profile found to be fraudulent',$action,'Auto Check : Cron Job');
			$arrReportedProfileIDs[] = $member['uid'];

			if($contents == 'Error:Connection' || $contents == 'Error:APIKey')
			{
				watchdog('cron', " $antoaCurrentProcess : [ Error Response : Connection error ] Error While reporting profile for fraudulent profile found. ");
				$connectionError = 1;
				variable_set('anatoa_cronConnectionError',1);
				variable_set('anatoa_cronRunning',0);
				return ;
			}
		}

		$extra = !empty($member['IPAddress']) ? 'WithIP' : '';

		$objDBModel->updateWithSoapCheckResult($member['uid'],$anatoaConfig->ProfileTable['Drupal']['priKey'],$member['uid'],$anatoaConfig->ProfileTable['Drupal']['tblName'],'Profile',$return,$fraudulent,$action,$extra);
		$objDBModel->enterLog('Check Profile','Auto Check : Cron Job',$member['uid'],$member['uid'],$anatoaConfig->ProfileTable['Drupal']['tblName'],addslashes(serialize($profileParams)),$return);

		if($fraudulent)
		{
			$userObj = (object) $member;
			module_invoke_all('anatoa', 'fradulent_profile_found', $userObj);
		}
	}



	///////////////////////   Update all Messages Table   /////////////////////////////////////////

	$antoaCurrentProcess = '( Anatoa Cron File -> checkMessage )';

	foreach ($anatoaConfig->Tables['Message'] as $msgTable)
	{
		$query = "SELECT {$msgTable['tblName']}.{$msgTable['priKey']} ,{$msgTable['Message']},{$msgTable['Sender']}
					FROM {$msgTable['tblName']}
					LEFT JOIN {$anatoaConfig->MasterTables['Message']} AS Master
							ON  Master.MessageID = {$msgTable['tblName']}.{$msgTable['priKey']} AND Master.TableName = '{$msgTable['tblName']}'
					WHERE {$msgTable['tblName']}.{$msgTable['Sender']} > 1 AND ( Master.ID IS NULL OR Master.CheckStatus = 'Unchecked' ) AND {$msgTable['Date']} > '$installationDate' ";

		$result = db_query($query);
		$messagesToUpdate = $objDBModel->getAllRecords($result);

		foreach ($messagesToUpdate as $tempMsg)
		{
			$query ='INSERT IGNORE INTO '.$anatoaConfig->MasterTables['Message'].' SET ProfileID='.$tempMsg[$msgTable['Sender']].', MessageID = '.$tempMsg[$msgTable['priKey']].', TableName = \''.$msgTable['tblName']."'";
			db_query($query);

			$messageParams = array( 'Message'=>stripslashes(strip_tags($tempMsg[$msgTable['Message']])) );

			if( !$soapclient->callMethod( $APIKey, 'checkMessage', 'Messages', array( 'Message'=>$messageParams ), $returnValues ) )
			{
				watchdog('cron', " $antoaCurrentProcess : [ Error Connection ] Unable to connect. ");
				$connectionError = 1;
				variable_set('anatoa_cronConnectionError',1);
				variable_set('anatoa_cronRunning',0);
				return ;
			}

			$return = $returnValues[0];

			$fraudulent = 0;
			$messageParams['APIKey'] = $APIKey;

			if(!is_numeric($return))
			{
				watchdog('cron', " $antoaCurrentProcess : [ Error Response : $return ] Error in processing you request. ");

				if($return == 'USAGE_COMPLETE')
				{
					$MessageUsageCompleted = 1;
					$exitOuterForLoop = 1;
					break;
				}
				if($errorResponse < $errorResponseThreshHold)
				{
					$errorResponse++;

					$query ='UPDATE '.$anatoaConfig->MasterTables['Message'].' SET CheckStatus = \'Failed Check\' ,checkReasons = \''.$return.'\' WHERE  ProfileID = '.$tempMsg[$msgTable['Sender']]." AND MessageID = {$tempMsg[$msgTable['priKey']]} AND TableName = '".$msgTable['tblName']."' ;";
					db_query($query);
					$objDBModel->enterLog('Check Message','Auto Check : Cron Job',$tempMsg[$msgTable['Sender']],$tempMsg[$msgTable['priKey']],$msgTable['tblName'],addslashes(serialize($messageParams)),$return);

					continue;
				}
				else
				{
					watchdog('cron', " $antoaCurrentProcess : [ Error Response : $return ] ");
					variable_set('anatoa_cronRunning',0);
					return ;
				}
			}

			elseif($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
			{
				$fm++;
				$fraudulent =1;
			}

			if(($return > 6.0) && (!in_array($tempMsg[$msgTable['Sender']],$arrReportedProfileIDs)))
			{
				$contents = $objWebService->reportProfile($tempMsg[$msgTable['Sender']],'Message found to be fraudulent',$action,'Auto Check : Cron Job',$tempMsg[$msgTable['priKey']].':'.$msgTable['tblName']);

				if($contents == 'Error:Connection' || $contents == 'Error:APIKey')
				{
					watchdog('cron', " $antoaCurrentProcess : [ Error Response : Connection error ] Error While reporting profile for fraudulent message sender. ");
					$connectionError = 1;
					variable_set('anatoa_cronConnectionError',1);
					variable_set('anatoa_cronRunning',0);
					return ;
				}

				$arrReportedProfileIDs[] = $tempMsg[$msgTable['Sender']];
			}

			$objDBModel->updateWithSoapCheckResult($tempMsg[$msgTable['Sender']],$msgTable['priKey'],$tempMsg[$msgTable['priKey']],$msgTable['tblName'],'Message',$return,$fraudulent,$action);
			$objDBModel->enterLog('Check Message','Auto Check : Cron Job',$tempMsg[$msgTable['Sender']],$tempMsg[$msgTable['priKey']],$msgTable['tblName'],addslashes(serialize($messageParams)),$return);

			if($fraudulent)
			{
				$userObj = db_fetch_object(db_query('SELECT * FROM {users} WHERE uid = %d',$tempMsg[$msgTable['Sender']]));
				module_invoke_all('anatoa', 'fradulent_message_found', $userObj);
			}
		}
		if ($exitOuterForLoop) break;
	}


	///////////////////////   Update all Image in  Table   /////////////////////////////////////////

	$antoaCurrentProcess = '( Anatoa Cron File -> Images )';
	$exitOuterForLoop = 0;

	foreach ($anatoaConfig->Tables['Images'] as $imgTable)
	{
		$joinString = !empty($imgTable['JOIN']) ? $imgTable['JOIN'] : '';

		$query = "SELECT {$imgTable['tblName']}.{$imgTable['priKey']},{$imgTable['Filename']}, {$imgTable['tblName']}.{$imgTable['MemberId']}
					FROM {$imgTable['tblName']}  $joinString LEFT JOIN {$anatoaConfig->MasterTables['Image']} AS Master
					ON  Master.ImageID = {$imgTable['tblName']}.{$imgTable['priKey']} AND Master.TableName = '{$imgTable['tblName']}'
				WHERE ( Master.ID IS NULL OR Master.CheckStatus = 'Unchecked')
					AND {$imgTable['tblName']}.{$imgTable['DateCreated']} > $installationDate
					AND {$imgTable['Filename']} != '' AND {$imgTable['Filename']} IS NOT NULL
					AND {$imgTable['tblName']}.{$imgTable['MemberId']} > 1"; // as MemberID

		$result = db_query($query);
		$imageToUpdate = $objDBModel->getAllRecords($result);

		foreach ($imageToUpdate as $tempImg)
		{
			$query = 'SELECT COUNT(*) FROM '.$anatoaConfig->MasterTables['Image'].' WHERE ProfileID='.$tempImg[$imgTable['MemberId']].' AND ImageID = '.$tempImg[$imgTable['priKey']].' AND TableName = \''.$imgTable['tblName']."'";

			if(db_result(db_query($query)) == 0)
			{
				$query ='INSERT INTO '.$anatoaConfig->MasterTables['Image'].' (ProfileID,ImageID,TableName)
							VALUES ('.$tempImg[$imgTable['MemberId']].','.$tempImg[$imgTable['priKey']].",'".$imgTable['tblName']."')";
				db_query($query);
			}


			$fileDir = dirname(__FILE__);
			$fileDir = str_replace(array('sites/all/modules/anatoa','sites\\all\\modules\\anatoa'),'',$fileDir);

			$file = $fileDir.$tempImg[$imgTable['Filename']];
			$id = $tempImg[$imgTable['priKey']];
			$idField = $imgTable['priKey'];

			if (file_exists($file))
			{
				$f1 = fopen($file,'r');
				$content = fread($f1,filesize($file));
				fclose($f1);

				$imageHash = md5($content);
				$imageParams = array( 'ImageHash'=>$imageHash );

				if( !$soapclient->callMethod( $APIKey, 'checkImage', 'Images', array( 'Image'=>$imageParams ), $returnValues ) )
				{
					watchdog('cron', " $antoaCurrentProcess : [ Error Connection ] Unable to connect to anatoa.com. ");
					$connectionError = 1;
					variable_set('anatoa_cronConnectionError',1);
					variable_set('anatoa_cronRunning',0);
					return ;
				}

				$return = $returnValues[0];
				$fraudulent = 0;
				$imageParams['APIKey'] = $APIKey;

				if(!is_numeric($return))
				{
					watchdog('cron', " $antoaCurrentProcess : [ Error Response : $return ] Error in processing you request. ");

					if($return == 'USAGE_COMPLETE')
					{
						$ImageUsageCompleted = 1;
						$exitOuterForLoop = 1;
						break;
					}
					if($errorResponse < $errorResponseThreshHold)
					{
						$errorResponse++;

						$query ='UPDATE '.$anatoaConfig->MasterTables['Image'].' SET CheckStatus = \'Failed Check\' ,checkReasons = \''.$return.'\' WHERE ProfileID = '.$tempImg[$imgTable['MemberId']]." AND ImageID = $id AND TableName = '".$imgTable['tblName']."';";

						db_query($query);

						$objDBModel->enterLog('Check Image','Auto Check : Cron Job',$tempImg[$imgTable['MemberId']],$id,$imgTable['tblName'],addslashes(serialize($imageParams)),$return);
						continue;
					}
					else
					{
						watchdog('cron', " $antoaCurrentProcess : [ Error Response : $return ] ");
						variable_set('anatoa_cronRunning',0);
						return ;
					}
				}
				elseif($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
				{
					$fi++;
					$fraudulent =1;
				}

				if(($return[0] > 6.0) && (!in_array($tempImg[$imgTable['MemberId']],$arrReportedProfileIDs)))
				{
					$contents = '';
					$contents = $objWebService->reportProfile($tempImg[$imgTable['MemberId']],'Image found to be fraudulent',$action,'Auto Check : Cron Job',$id.':'.$imgTable['tblName']);

					if($contents == 'Error:Connection' || $contents == 'Error:APIKey')
					{
						watchdog('cron', " $antoaCurrentProcess : [ Error Response : Connection error ] Error While reporting profile for fraudulent image. ");
						$connectionError = 1;
						variable_set('anatoa_cronConnectionError',1);
						variable_set('anatoa_cronRunning',0);
						return ;
					}

					$arrReportedProfileIDs[] = $tempImg[$imgTable['MemberId']];
				}

				$objDBModel->updateWithSoapCheckResult($tempImg[$imgTable['MemberId']],$idField,$id,$imgTable['tblName'],'Image',$return,$fraudulent,$action,$tempImg[$imgTable['Filename']]);
				$objDBModel->enterLog('Check Image','Auto Check : Cron Job',$tempImg[$imgTable['MemberId']],$id,$imgTable['tblName'],addslashes(serialize($imageParams)),$return);

				if($fraudulent)
				{
					$userObj = db_fetch_object(db_query('SELECT * FROM {users} WHERE uid = %d',$tempImg[$imgTable['MemberId']]));
					module_invoke_all('anatoa', 'fradulent_image_found', $userObj);
				}
			}
			else
			{
				$query ='UPDATE '.$anatoaConfig->MasterTables['Image'].' SET CheckStatus = \'Failed Check\' ,checkReasons = \'File Not Found\' WHERE TableName = \''.$imgTable['tblName']."' AND ImageID = $id;";
				db_query($query);
				watchdog('cron', " $antoaCurrentProcess : File Not Found ".$file);
			}
		}
		if ($exitOuterForLoop) break;
	}

	$usageTypes = array('Profile','Message','Image');
	$lastLogCleared  = variable_get('anatoa_lastLogCleared',0);

	if($lastLogCleared < strtotime('-15 days'))
	{
		db_query('DELETE FROM anatoa_transactionlog WHERE (TransactionType = \'Check Image\' OR TransactionType = \'Check Message\') AND LogDate < \''.strtotime('-1 Month').'\'');

		variable_set('anatoa_lastLogCleared',time());
	}

	if($connectionError)
	variable_set('anatoa_Error',$anatoaConfig->genMsg['CronConnectionError']);
	else
	variable_set('anatoa_Error','');

	foreach ($usageTypes as $type)
	{
		$varName = $type.'UsageCompleted';
		if($$varName)
		variable_set("Check{$type}_limit_Reached",1);
		else
		variable_set("Check{$type}_limit_Reached",0);
	}

	variable_set('anatoa_CronRunnedAt',time());
	variable_set('anatoa_cronRunning',0);

	watchdog('cron', " Anatoa Script for cron finished........");

}