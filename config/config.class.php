<?php
/* Anatoa Drupal Plugin v1.0
*  Copyright 2009 Univalence Ltd
*  All rights reserved
*
* DISCLAIMER
* Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
* (whether such damage or losses were foreseen, foreseeable, known or otherwise):
* (a) loss of data;
* (b) loss of revenue or anticipated profits;
* (c) loss of business;
* (d) loss of opportunity;
* (e) loss of goodwill or injury to reputation;
* (f) losses suffered by third parties; or
* (g) any indirect, consequential, special or exemplary damages
* arising from the use of Anatoa.com or associated web services regardless of the
* form of action, including the use of this source code.
*/

class anatoaConfiguration
{
	public $soapURL;
	public $anatoaURL;
	public $drupalURLPrefix;
	public $serverIPAddress;
	public $tempDir;
	public $pluginName;
	public $pluginVersion;
	public $installationInstructions;

	//public $impDates  = array();
	public $ProfileTable = array();
	public $MasterTables = array();
	public $Tables = array();

	public $genMsg = array();
	public $soapResponse = array();
	public $rejectionThreshold = array();
	public $reasonForProfileDeletion = array();
	public $reasonForReportingProfile = array();
	public $reqMod = array();

	function __construct()
	 {
	 	$this->soapURL = 'http://ws.anatoa.com/ws/?wsdl';
		$this->anatoaURL = "<a href='http://www.anatoa.com' target='_blank'>Anatoa.com</a>";
		$this->drupalURLPrefix = 'admin/user/';
		$this->tempDir = "/tmp";
		$this->pluginName = "Drupal";
		$this->pluginVersion = "1.0";
		$this->serverIPAddress = $_SERVER['SERVER_ADDR'];
		$this->installationInstructions = "<a href='installation_instructions.php'>here</a>";

		$this->soapResponse = array(
					'VALID_API_KEY' => t("Your API Key has been verified and found to be correct by $this->anatoaURL."),
					'INVALID_API_KEY' => t("Your API Key has been verified and found to be incorrect by $this->anatoaURL."),
					'USER_INACTIVE' => t("Your account on anatoa in inactive, please activated it."),
					'USER_SUSPENDED' => t("Your account on anatoa in suspended"),
					'IPADDRESS_APIKEY_MISMATCH' => t("The IP address of the current website($this->serverIPAddress) does not match the address registered on $this->anatoaURL for the given API key. Please correct this to activate the Anatoa plugin "),
					'USAGE_COMPLETE' => t("You have reached the maximum number of Anatoa {--ServiceType--} checks for the current month. Please go to $this->anatoaURL and upgrade your subscription in order to keep your members protected.")
					);

		$this->genMsg = array(
					'AllProfileCheck' => t('All profiles have been checked.'),
					'noResponse' => t('Failed to verify APIKey from Anatoa.com via SOAP Webservice.'),
					'APIKeySaved' => t("API Key tested and Saved Successfully"),

					'APIKeyNotConfigured' => t('API Key is not configured. Please, configure it for proper working of this plugin.'),
					'APIKeyInvalid' => t("API is not valid. Please, check in your account on http://www.anatoa.com"),
					'ConnectionError' => t("Unable to connect to web server, please check your connection or setting. Process Stopped"),

					'InstallationError' => t("Anatoa plugin is not correctly installed - click $this->installationInstructions for installation page."),
					'CronNotRunned' => t("Anatoa plugin cron job has not been set up - click $this->installationInstructions for instructions on how to do this."),
					'CronConnectionError' => t("&lt Cron Job &gt : Currently unable to connect to $this->anatoaURL - queueing requests for later retry.")
					);

		$this->rejectionThreshold = array('Low' => 3.0,'Standard' => 6.0,'High' => 9.0,'Disable'=>10.0);

		$this->reasonForProfileDeletion = array('Profile' => 'Suspected fraudulent profile',
												'Message' => 'Suspected fraudulent message',
												'Image' => 'Suspected fraudulent image'
												);

		$this->reasonForReportingProfile = array(t('Abused other member'),
												t('Asked member for money'),
												t('Membership fee fraud'),
												t('Posted inappropriate content'),
												t('Spammed other members'),
												t('Other')
												);

		$this->ProfileTable = array(
							'Drupal' => array(
										'tblName'=>'users',
										'priKey'=>'uid',
										'UserName'=>'name',
										'Password'=>'pass',
										'Email'=>'mail',
										'DateCreated'=>'created',
										'LastLogin'=>'login'
										),
						  	'Anatoa' => array(
										'tblName'=>'anatoa_users',
										'priKey'=>'uid',
										'UserName'=>'name',
										'Password'=>'pass',
										'Email'=>'mail',
										'DateCreated'=>'created',
										'LastLogin'=>'login'
										)
								);

		$this->MasterTables = array('Profile' => 'anatoa_mst_profiles',
					  			'Message' => 'anatoa_mst_messages',
					  			'Image'	=> 'anatoa_mst_images'
								);

		$this->Tables['Images'][] =	array(		'tblName'=>'users',
												'priKey'=>'uid',
												'MemberId'=>'uid',
												'Filename'=>'picture',
												'Title'=> 'name',
												'DateCreated'=>'created',
												'Status'=>'status',
												'JOIN'=> '',
												'REQUIRED' => " (picture IS NOT NULL OR picture != '') ",
												'Dependent'=> ''
									);

		if(db_result(db_query('SELECT status FROM system WHERE name = \'anatoa_comment\'')) == 1 )
			{
			  $this->Tables['Message'][] = array(
												'tblName'=>'comments',
												'priKey'=>'cid',
												'Sender'=>'uid',
												'Subject'=>'subject',
												'Message'=>'comment',
												'Date'=>'timestamp'
											);
			}

		if(db_result(db_query('SELECT status FROM system WHERE name = \'anatoa_privatemsg\'')) == 1 )
			{
			  $this->Tables['Message'][] = array(
												'tblName'=>'pm_message',
												'priKey'=>'mid',
												'Sender'=>'author',
												'Subject'=>'subject',
												'Message'=>'body',
												'Date'=>'timestamp',
												'Dependent'=>array(
															'tblNames'=>'pm_message,pm_index',
															'JOIN'=>'LEFT JOIN pm_index ON pm_index.mid = pm_message.mid')
											);
			}

		if(db_result(db_query('SELECT status FROM system WHERE name = \'anatoa_image\'')) == 1 )
			{
			  $this->Tables['Images'][] = array(
											'tblName'=>'node',
											'priKey'=>'nid',
											'MemberId'=>'uid',
											'Filename'=>'filepath',
											'Title'=>'title',
											'DateCreated'=>'created',
											'Status'=>'status',
											'JOIN'=>"LEFT JOIN image ON image.nid = node.nid AND image.image_size = '_original'
													 LEFT JOIN files ON files.fid = image.fid ",
											'REQUIRED' => " node.type = 'image' ",
											'Dependent'=>array(
													'tblNames'=>'node,image,files',
													'JOIN'=>'LEFT JOIN image ON image.nid = node.nid
															 LEFT JOIN files ON files.fid = image.fid ')
										);
			}

		if(db_result(db_query('SELECT status FROM system WHERE name = \'anatoa_upload\'')) == 1 )
			{
			  $this->Tables['Images'][] = array(
												'tblName'=>'node',
												'priKey'=>'nid',
												'MemberId'=>'uid',
												'Filename'=>'filepath',
												'Title'=>'title',
												'DateCreated'=>'created',
												'Status'=>'status',
												'JOIN'=>"LEFT JOIN upload ON upload.nid = node.nid
														 LEFT JOIN files ON files.fid = upload.fid AND files.filemime LIKE 'image%' ",
												'REQUIRED' => '',
												'Dependent'=>array(
														'tblNames'=>'node,upload,files',
														'JOIN'=>'LEFT JOIN upload ON upload.nid = node.nid
																 LEFT JOIN files ON files.fid = upload.fid AND files.filemime LIKE \'image%\' ')
											);
			}

		$this->reqMod['anatoa_addresses_user'] = db_result(db_query('SELECT status FROM system WHERE name = \'anatoa_addresses_user\''));

	 }

	// this implements the 'singleton' design pattern.
	function getInstance()
    {
        static $instance;
        if (!isset($instance)) {
            $c = __CLASS__;
            $instance = new $c;
        }
        return $instance;
    }

}