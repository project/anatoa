<?php
/**
 *  Anatoa Drupal Plugin
 *  Copyright 2009 Univalence Ltd
 *  All rights reserved
 *
 * DISCLAIMER
 * Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
 * (whether such damage or losses were foreseen, foreseeable, known or otherwise):
 * (a) loss of data;
 * (b) loss of revenue or anticipated profits;
 * (c) loss of business;
 * (d) loss of opportunity;
 * (e) loss of goodwill or injury to reputation;
 * (f) losses suffered by third parties; or
 * (g) any indirect, consequential, special or exemplary damages
 * arising from the use of Anatoa.com or associated web services regardless of the
 * form of action, including the use of this source code.
 */

/**
 * This Class intracts with database to get or set required fields
 *
 */
class dbModel
{

	public $profile_ids = array();
	public $returnValue = 0;
	public $strProfileIds;

	/**
	 * Get All rows from a result set
	 *
	 * @param resultSet $resultSet
	 * @return associated array of Records
	 */
	function getAllRecords($resultSet)
	{
		$arrReturn = array();
		while ($row = db_fetch_array($resultSet))
			$arrReturn[] = $row;
		return $arrReturn;
	}

	/**
	 * Get API Detail for plugin
	 *
	 * @return Array of configuration fields
	 */
	function getAPIConfig()
	{
		$res = db_query("SELECT * FROM anatoa_config LIMIT 1;");
		return db_fetch_array($res);
	}

	/**
	 * Get Field List for Profile (Admin defined extra fields)
	 *
	 * @return array of Fields
	 */
	function getFieldList()
	{
		$arrFields = array();
		$anatoaFields = array('First Name'=>'0','Last Name'=>'0','Country'=>'0');
		$res = db_query('SELECT fid,title,name FROM {profile_fields};');

		while( $row = db_fetch_array($res))
		{
			$arrFields[0][$row['title']] = $row['fid'];

			if(strpos(strtolower($row['title']),'first name') !== false)
			 	$anatoaFields['First Name'] = $row['fid'] ;
			elseif(strpos(strtolower($row['title']),'last name') !== false)
			 	$anatoaFields['Last Name'] = $row['fid'] ;
			elseif(strpos(strtolower($row['title']),'country') !== false)
			 	$anatoaFields['Country'] = $row['fid'] ;

			$arrFields[2][$row['fid']] = $row['name'];
		}

		$arrFields[1] = $anatoaFields;

		return $arrFields;
	}

	/**
	 * Gets Number of Old Profiles to be check
	 *
	 * @param int $maxLimit
	 * @param boolean $recheck
	 * @return int (Number of Profiles)
	 */
	function getNumberProfileUnchecked($maxLimit=250,$recheck='')
 	{
 		$anatoaConfig = anatoaConfiguration::getInstance();
 		$installationDate = variable_get('anatoa_installationDate',time());
		if($installationDate == 0 )
			 $installationDate = time();

 		$strRecheckCondition = $recheck == 'Recheck' ?  "OR ( Master.CheckedWithIP = 'No' AND
														Master.checkStatus = 'Checked' AND
														Master.Status !='Deleted' AND
														Master.IPAddress != '' AND
														Master.IPAddress IS NOT NULL)"
 													 :  '' ;

 		if(!empty($maxLimit))
 			$maxLimit = "LIMIT $maxLimit";
 	    else $maxLimit = '';

 		$query = "SELECT ProfileTbl.{$anatoaConfig->ProfileTable['Drupal']['priKey']} FROM {$anatoaConfig->ProfileTable['Drupal']['tblName']} AS ProfileTbl
				  LEFT JOIN {$anatoaConfig->MasterTables['Profile']} AS Master ON  ProfileTbl.{$anatoaConfig->ProfileTable['Drupal']['priKey']} = Master.ProfileID
				  WHERE	(( Master.CheckStatus='Unchecked' AND Master.Status != 'Deleted' ) OR Master.ID IS NULL $strRecheckCondition) AND
				  		ProfileTbl.{$anatoaConfig->ProfileTable['Drupal']['DateCreated']} < $installationDate  AND
				  		ProfileTbl.{$anatoaConfig->ProfileTable['Drupal']['priKey']} > 1
				  ORDER BY Master.IPAddress DESC $maxLimit";

		$result = db_query($query);
		$this->profile_ids = array();

		while($row = db_fetch_array( $result )) $this->profile_ids[] = $row;
		$this->getStrProfileIds();
		return count($this->profile_ids);
 	}

 	function getStrProfileIds()
 	{
 		$anatoaConfig = anatoaConfiguration::getInstance();

 		$strComma = '';
 		$this->strProfileIds = '';
 		foreach ($this->profile_ids as $arrIds)
 		{
 			$this->strProfileIds .= $strComma.$arrIds[$anatoaConfig->ProfileTable['Drupal']['priKey']];
 			$strComma=',';
 		}
 	}

	/**
	 * Get user profile fields
	 *
	 * @param int $userId
	 * @param String $strFieldIDs (field ID's as string)
	 * @return Array values
	 */
	function getCustomProfileFieldData($userId,$strFieldIDs = '')
	{
		$arrReturn = array('0'=>'');
		$query = "SELECT fid,value FROM {profile_values} WHERE uid = $userId AND fid IN ($strFieldIDs);";
		$result = db_query($query);

		while ($row = db_fetch_array($result))
		  $arrReturn[$row['fid']]= $row['value'];

		return $arrReturn;
	}

	function updateWithSoapCheckResult($memberId, $idField, $id, $table, $mstTable, $result, $fraudulent, $action, $file='')
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
		global $base_url;

		$extraField = (!empty($file) && $file != 'WithIP') ? ', Master.ImageFile = \''.addslashes($base_url.'/'.$file)."' " : '';
		$extraField = (!empty($file) && $file == 'WithIP') ? ', Master.CheckedWithIP =\'Yes\'' : $extraField;

		if($fraudulent != 1)
			$action = 'Unconfirmed';

		$insertProfileIDColumn = ($table != 'users') ? " ProfileID," : '';
		$insertprofileID = ($table != 'users') ? " $memberId," : '';

		$query = "SELECT COUNT(*) FROM {$anatoaConfig->MasterTables[$mstTable]} WHERE {$mstTable}ID = $id AND TableName = '$table'";
		if(db_result(db_query($query)) == 0)
    	 {
			db_query("INSERT INTO {$anatoaConfig->MasterTables[$mstTable]} ($insertProfileIDColumn {$mstTable}ID, TableName) VALUES ($insertprofileID $id, '$table')");
		  }

		$query = "UPDATE $table
					INNER JOIN {$anatoaConfig->MasterTables[$mstTable]} AS Master ON Master.{$mstTable}ID = $table.$idField
					SET
					 Master.Status = '$action' ,
					 Master.CheckStatus='Checked',
					 Master.checkDate = ".time().",
					 Master.checkReturnValue='$result',
					 Master.checkReasons = 'before Plugin Installed',
					 Master.checkMode = 'Manul : Admin' $extraField
					 Where $table.$idField = '$id' AND Master.TableName = '$table'";
		db_query( $query );

		if(db_affected_rows() == 0)
		  	db_query(str_replace("$table","anatoa_$table",$query));

		if($action == 'Delete')
		{
			$query= "SELECT * FROM $table WHERE $idField = $id ";
			$tableData = db_fetch_array($query);

			if($table == $anatoaConfig->ProfileTable['Drupal']['tblName'])
				$Anatoatable = $anatoaConfig->ProfileTable['Anatoa']['tblName'];
			else $Anatoatable = 'anatoa_'.$table;

			if(!empty($tableData))
			{
				$query = 'INSERT INTO '.$Anatoatable ;
				$comma = '(';
				foreach ($tableData as $key => $value)
				{
					$query .= $comma.$key;
					$comma = ',';
				}
				$query .= ') VALUES ';
				$comma = '(';
				foreach ($tableData as $key => $value)
				{
					$query .= $comma.'\''.addslashes($value).'\'';
					$comma = ',';
				}
				$query .= ')';
				db_query($query);
			}
			$query = "DELETE FROM $table WHERE $idField='$id';";
			db_query($query);

		}
		elseif ($action == 'Block' && $table == 'users')
		{
			//db_query("UPDATE users SET status = 0 WHERE $idField = '$id';");
			$temp = (object) array('uid'=>$id);
			user_block_user_action($temp);
		}
	}

	function updateWithSoapReportResult($memberId,$idField,$id,$table,$mstTable,$result,$reason ='Reported By Admin',$file='',$mode='Suspended',$calledBy = '',$exception=':')
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
		global $base_url;
		$extraField = (!empty($file)) ? ', Master.ImageFile = \''.addslashes($base_url.'/'.$file)."' " : '';

		$exceptional = explode(':',$exception);

		$insertProfileIDColumn = ($table != 'users') ? " ProfileID," : '';
		$insertprofileID = ($table != 'users') ? " $memberId," : '';

		$query = "SELECT COUNT(*) FROM {$anatoaConfig->MasterTables[$mstTable]} WHERE {$mstTable}ID = $id AND TableName = '$table'";
		if(db_result(db_query($query)) == 0)
    	 {
			db_query("INSERT INTO {$anatoaConfig->MasterTables[$mstTable]} ($insertProfileIDColumn {$mstTable}ID, TableName) VALUES ($insertprofileID $id, '$table')");
		  }

		// Store the fact that this profile/image/message has been reported
		$query = '';
		$query = "UPDATE $table
					INNER JOIN {$anatoaConfig->MasterTables[$mstTable]} AS Master
					ON Master.{$mstTable}ID = $table.$idField AND Master.TableName = '$table'
					 SET
					 Master.Status = '$mode' ,
					 Master.ReportStatus='Reported',
					 Master.reportDate = ".time().",
					 Master.reportReturnValue='$result',
					 Master.reportReasons  = '$reason',
					 Master.reportMode = 'Manul : Admin' $extraField
					 Where $table.$idField = '$id' AND Master.TableName = '$table'";
		db_query( $query );

		// Delete or block the profile
		if($exceptional[0] != $id || $exceptional[1]!= $table)
		{
			if($mode == 'Delete')
			{
				$query= "SELECT * FROM $table WHERE $idField = $id ";
				$member = db_fetch_array(db_query($query));

				if($table == $anatoaConfig->ProfileTable['Drupal']['tblName'])
					$Anatoatable = $anatoaConfig->ProfileTable['Anatoa']['tblName'];
				else $Anatoatable = 'anatoa_'.$table;

				if(!empty($member))
				{
					$query = 'INSERT INTO '.$Anatoatable ;
					$comma = '(';
					foreach ($member as $key => $value)
					{
						$query .= $comma.$key;
						$comma = ',';
					}
					$query .= ') VALUES ';
					$comma = '(';
					foreach ($member as $key => $value)
					{
						$query .= $comma.'\''.addslashes($value).'\'';
						$comma = ',';
					}
					$query .= ')';
					db_query($query);
				}
				$query = "Delete From $table WHERE $idField=$id";
				db_query($query);
			}
			elseif ($mode == 'Block' && $table == 'users')
			{
				//db_query("UPDATE users SET status = 0 WHERE $idField = '$id'");
				$temp = (object) array('uid'=>$id);
				user_block_user_action($temp);
			}
		}
	}

	/**
	 * Enter Log into database about Web Sevice used
	 *
	 * @param String $type (type of webservice used)
	 * @param int $by (calling user id)
	 * @param int $memberId (associated member id of message,image or profile)
	 * @param int $recordId (record id withing actual table)
	 * @param String $tableName (table name)
	 * @param String $para (serialized varible for parameter passed with SOAP Service)
	 * @param String $result (result returned by SOAP service)
	 * @param String $reason (reason behind calling report or check web service)
	 */
	function enterLog($type,$by,$memberId,$recordId,$tableName,$para,$result,$reason='')
	{
		$query= "UPDATE anatoa_transactionlog SET calledBy = '$by',	TransactionType = '$type', MemberId = $memberId, parameters = '$para', ReturnValue='$result', Reasons='$reason', LogDate=".time()." WHERE MemberId = $memberId AND RecordID='$recordId' AND TableName = '$tableName' AND TransactionType = '$type';";
		db_query($query);

		if(db_affected_rows()==0)
		{
			$query = "INSERT INTO anatoa_transactionlog SET	calledBy = '$by', RecordID='$recordId',	TableName = '$tableName', TransactionType = '$type', MemberId = $memberId, parameters = '$para', ReturnValue='$result',	Reasons='$reason', LogDate=".time()."";
			db_query($query);
		}
	}

	/**
	 * Statistics about webserived used in last 24 hours
	 *
	 * @return Array of integer for different webservice used
	 */
	function getlast24hrStat()
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
		$resultSet = $this->getAPIConfig();
		$APIKey = $resultSet['APIKey'];
		$rejection_threshold = $resultSet['Rejection_Threshold'];

		foreach ($anatoaConfig->MasterTables as $type => $tableName)
		{
			$temp["Checked$type"] = db_result(db_query("SELECT count(*) FROM ".$tableName." WHERE CheckStatus = 'Checked' AND checkDate > '".strtotime('- 24 hours')."'"));

			$temp["Fraudulent$type"] = db_result(db_query("SELECT count(*) FROM ".$tableName." WHERE CheckStatus = 'Checked' AND  checkReturnValue > {$anatoaConfig->rejectionThreshold[$rejection_threshold]} AND checkDate > '".strtotime('- 24 hours')."'"));
		}

		$query = "SELECT count(*) FROM ".$anatoaConfig->MasterTables['Profile']." WHERE reportReturnValue = 'SUBMITTED_PROFILE_SUCCESS' AND reportDate > '".strtotime('- 24 hours')."'";
		$temp["ReportedProfiles"] = db_result(db_query($query));

		return $temp;
	}

	/**
	 * Get Member List
	 *
	 * @param Array (Strings) $fieldLists (Field to be selected)
	 * @param Array (Strings) $where (WHERE condition)
	 * @param int $intPage (Current Page Number)
	 * @param String $strSortBy (Sort by Field Name)
	 * @param String $strOrder (Sorting Order [ASC | DESC]))
	 * @param int $RecordPerPage (records per page)
	 * @param String $extra (extra parameter like 'all' to count number of records)
	 * @return Array(associative) of records
	 */
	function memberList($fieldLists,$where,$intPage=0,$strSortBy='',$strOrder='',$RecordPerPage=30,$extra = '')
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

       $arrReturn = array();
       $Limit = '';
       $strWhereConditions = '';
       $order = '';
       $Start = $intPage * $RecordPerPage;

       if($extra == '')
          $Limit = "LIMIT $Start,$RecordPerPage;";

       $fieldTable = array( 'uid'=>$anatoaConfig->ProfileTable['Drupal']['tblName'],
       						'name'=>$anatoaConfig->ProfileTable['Drupal']['tblName'],
       					    'status'=>$anatoaConfig->ProfileTable['Drupal']['tblName'],
       						'IPAddress'=>'Master',
       						'ProxyIP'=> 'Master'
					       );

       if(isset($fieldTable[$strSortBy]) && isset($strSortBy))
			$strSortBy = "$fieldTable[$strSortBy].$strSortBy";

       $queryFields = '';
       $separator = '';
       foreach ($fieldLists as $field)
       {
       	  if(isset($fieldTable[$field]))
			$queryFields .=  $separator."$fieldTable[$field].$field as $field";

		  else
       	  	$queryFields .= $separator." $field ";
       	  $separator = ',';
       }

       $where[] = "{$anatoaConfig->ProfileTable['Drupal']['tblName']}.{$anatoaConfig->ProfileTable['Drupal']['priKey']} != 1";

       if(!empty($where))
       		$strWhereConditions = "WHERE ".implode(' AND ',$where);

       if(!empty($strSortBy))
       		$order = "ORDER BY $strSortBy $strOrder";

       if($extra == 'all')
         $queryFields = 'COUNT(*) AS RowCount';

       $query = "SELECT $queryFields FROM {$anatoaConfig->ProfileTable['Drupal']['tblName']}
       				LEFT JOIN {$anatoaConfig->MasterTables['Profile']} AS Master ON Master.ProfileID = {$anatoaConfig->ProfileTable['Drupal']['tblName']}.{$anatoaConfig->ProfileTable['Drupal']['priKey']}
					$strWhereConditions $order $Limit";

       return $this->getAllRecords(db_query($query));
	}

	function getMemberListForUpdating()
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
    	$installationDate = variable_get('anatoa_installationDate',0);

    	$this->fields = array($anatoaConfig->ProfileTable['Drupal']['tblName'].'.*','Master.IPAddress','Master.ProxyIP');
    	$this->strWhere = array("( Master.ID IS NULL OR Master.CheckStatus = 'Unchecked')"," {$anatoaConfig->ProfileTable['Drupal']['DateCreated']} > $installationDate");
		return $this->memberList($this->fields,$this->strWhere,'','','','','List');
	}

	/**
	 *  Get Profile Detail record for database
	 *
	 * @param int $profileId (Profile ID)
	 * @return Array(associative) of profile record
	 */
	function getProfileDetails($profileId)
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

 		$query ="SELECT {$anatoaConfig->ProfileTable['Drupal']['tblName']}.*,
 					Master.IPAddress, Master.ProxyIP,Master.Country, {$anatoaConfig->ProfileTable['Anatoa']['tblName']}.mail AS altEmail
 					FROM {$anatoaConfig->ProfileTable['Drupal']['tblName']}
 				 LEFT JOIN {$anatoaConfig->MasterTables['Profile']} AS Master ON Master.ProfileID = {$anatoaConfig->ProfileTable['Drupal']['tblName']}.{$anatoaConfig->ProfileTable['Drupal']['priKey']}
 				 LEFT JOIN {$anatoaConfig->ProfileTable['Anatoa']['tblName']} ON {$anatoaConfig->ProfileTable['Anatoa']['tblName']}.{$anatoaConfig->ProfileTable['Anatoa']['priKey']} = {$anatoaConfig->ProfileTable['Drupal']['tblName']}.{$anatoaConfig->ProfileTable['Drupal']['priKey']}
 				 WHERE {$anatoaConfig->ProfileTable['Drupal']['tblName']}.{$anatoaConfig->ProfileTable['Drupal']['priKey']} = $profileId;";

 		return db_fetch_array(db_query($query));
	}

	function getAllImagesOfProfile($profileId,$chkOrRpt = 'Check')
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
		$imageArray = array();
		$chkRptWhere = ($chkOrRpt == 'Check') ? "Master.CheckStatus='Unchecked'" : "Master.ReportStatus='Unreported'" ;

		$i = 0;
		if(!empty($anatoaConfig->Tables['Images']))
		{
			foreach ($anatoaConfig->Tables['Images'] as $imgTables)
	 		{
	 			 $query = "
					SELECT {$imgTables['tblName']}.{$imgTables['priKey']}, {$imgTables['Filename']} ,
							'{$imgTables['tblName']}' AS tableName, $i as tblIndex
					FROM {$imgTables['tblName']}
					LEFT JOIN {$anatoaConfig->MasterTables['Image']} AS Master
								ON  {$imgTables['tblName']}.{$imgTables['priKey']} = Master.ImageID AND
		      						  Master.TableName = '{$imgTables['tblName']}'";

				if($imgTables['JOIN']!='')
		      		$query .= $imgTables['JOIN'];

		      	$query .= " WHERE (	( $chkRptWhere AND
		      						  Master.Status != 'Deleted'
		      						) OR Master.ID IS NULL
		      					  )	AND {$imgTables['tblName']}.{$imgTables['MemberId']} IN ( $profileId )";

		      	if($imgTables['REQUIRED'] != '')
		      		$query .= ' AND '.$imgTables['REQUIRED'];

		      	$r = array();
		      	$result = db_query( $query );
		      	while( $r = db_fetch_array( $result ) )
			 		$imageArray[] =  $r;

			 	++$i;
	 		}
		}
 		return $imageArray;
	}

	function getAllMessagesOfProfile($profileId,$chkOrRpt = 'Check')
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
 		$msgArray = array();
 		$chkRptWhere = ($chkOrRpt == 'Check') ? "Master.CheckStatus='Unchecked'" : "Master.ReportStatus='Unreported'" ;
		$i = 0;

		if(!empty($anatoaConfig->Tables['Message']))
		{
			foreach ($anatoaConfig->Tables['Message'] as $msgTables)
	 		{
				$query = "SELECT {$msgTables['tblName']}.{$msgTables['priKey']},{$msgTables['Message']},
				 				'{$msgTables['tblName']}' AS tableName,	$i as tblIndex
							FROM {$msgTables['tblName']}
							LEFT JOIN {$anatoaConfig->MasterTables['Message']} as Master ON
								 {$msgTables['tblName']}.{$msgTables['priKey']} = Master.MessageID AND
								 Master.TableName = '{$msgTables['tblName']}'
						    WHERE (	( $chkRptWhere AND
		      						  Master.Status != 'Deleted'
		      						) OR Master.ID IS NULL
		      					  )	AND {$msgTables['tblName']}.{$msgTables['Sender']} IN ( $profileId )";

			 	$r = array();
		      	$result = db_query( $query );
		      	while( $r = db_fetch_array( $result ) )
			 		$msgArray[] =  $r;
			 	++$i;
	 		}
		}
		return $msgArray;
	}

	function restoreMember($MemberIn)
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

		$this->restoreMembersData($MemberIn);
		$query="UPDATE {$anatoaConfig->MasterTables['Profile']}
					LEFT JOIN {$anatoaConfig->ProfileTable['Anatoa']['tblName']}
							ON {$anatoaConfig->MasterTables['Profile']}.ProfileID = {$anatoaConfig->ProfileTable['Anatoa']['tblName']}.{$anatoaConfig->ProfileTable['Anatoa']['priKey']}
				SET {$anatoaConfig->MasterTables['Profile']}.Status = 'Restored'
				WHERE {$anatoaConfig->MasterTables['Profile']}.ProfileID IN ($MemberIn)";
		 $result = db_query($query);
	}

	function restoreMembersData($MemberIn)
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

		$query = "SELECT * FROM {$anatoaConfig->ProfileTable['Anatoa']['tblName']} WHERE {$anatoaConfig->ProfileTable['Anatoa']['priKey']} IN ($MemberIn)";
		$result = db_query( $query );

		while ($member = db_fetch_array( $result ))
		{
			$comma = '';
		    $query = 'SELECT count(*) FROM '.$anatoaConfig->ProfileTable['Drupal']['tblName']." WHERE {$anatoaConfig->ProfileTable['Anatoa']['priKey']} = ".$member[$anatoaConfig->ProfileTable['Drupal']['priKey']];

		   if(db_result(db_query($query)) == 0)
		   {
			   $fieldList = '';
			   foreach ($member as $fieldName=>$values)
				{
					$fieldList .= " $comma $fieldName ";
					$comma = ',';
				}

				$comma = '';

				$fieldListValues = '';
				foreach ($member as $fieldName=>$values)
				{
					$fieldListValues .= $comma."'".addslashes($values)."' ";
					$comma = ',';
				}

				db_query( 'INSERT INTO '.$anatoaConfig->ProfileTable['Drupal']['tblName'].'('.$fieldList.') VALUES ('.$fieldListValues.')' );
		   }
		}
	}

 	function getNumberImageUnchecked()
 	{
 		$anatoaConfig = anatoaConfiguration::getInstance();

		$this->returnValue = 0;
		if(!empty($anatoaConfig->Tables['Images']))
		{
	 		foreach ($anatoaConfig->Tables['Images'] as $imgTables)
	 		{
				$query = "SELECT count(*) AS noOfImages FROM {$imgTables['tblName']}
							LEFT JOIN {$anatoaConfig->MasterTables['Image']} AS Master
								ON  {$imgTables['tblName']}.{$imgTables['priKey']} = Master.ImageID  AND Master.TableName = '{$imgTables['tblName']}' ";

	 			if($imgTables['JOIN']!='')
	 	      		$query .= $imgTables['JOIN'];

		      	$query .= " WHERE ( Master.ID IS NULL OR ( Master.CheckStatus='Unchecked' AND Master.Status != 'Deleted' ))
		      					AND {$imgTables['tblName']}.{$imgTables['MemberId']} IN ( $this->strProfileIds )";

		      	if($imgTables['REQUIRED'] != '')
		      		$query .= ' AND '.$imgTables['REQUIRED'];

			 	$this->returnValue += db_result(db_query($query));
	 		}
		}

		return $this->returnValue;
 	}

 	function getNumberMessageUnchecked()
 	{
		$anatoaConfig = anatoaConfiguration::getInstance();
 		$this->returnValue = 0;

 		if(!empty($anatoaConfig->Tables['Message']))
		{
	 		foreach ($anatoaConfig->Tables['Message'] as $msgTables)
	 		{
				$query = "SELECT COUNT(*) AS noOfMessages FROM {$msgTables['tblName']} AS Orginal
							LEFT JOIN {$anatoaConfig->MasterTables['Message']} AS Master
								ON  Orginal.{$msgTables['priKey']} = Master.MessageID
								AND Master.TableName = '{$msgTables['tblName']}'
						    WHERE ( Master.ID IS NULL OR ( Master.CheckStatus='Unchecked' AND Master.Status != 'Deleted' ))
								AND Orginal.{$msgTables['Sender']} IN ( $this->strProfileIds )";

			 	$this->returnValue += db_result(db_query($query));
	 		}
		}
		return $this->returnValue;
 	}

	function getArchiveMemberData($id)
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

		$query = 'SELECT ProfileTable.*,Master.IPAddress ,Master.ProxyIP ,Master.reportReasons FROM '.$anatoaConfig->ProfileTable['Anatoa']['tblName'].' AS ProfileTable
						LEFT JOIN '.$anatoaConfig->MasterTables['Profile']." AS Master ON Master.ProfileID = ProfileTable.{$anatoaConfig->ProfileTable['Anatoa']['priKey']}
						WHERE ProfileTable.".$anatoaConfig->ProfileTable['Anatoa']['priKey']." = $id";

		return db_fetch_array(db_query(($query)));
	}

	function getMessagesOfDeletedProfile($profileId)
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
 		$msgArray = array();

 		if(!empty($anatoaConfig->Tables['Message']))
		{
	 		foreach ($anatoaConfig->Tables['Message'] as $msgTables)
	 		{
				$query = "SELECT  anatoa_{$msgTables['tblName']}.{$msgTables['Subject']}  AS Subject ,anatoa_{$msgTables['tblName']}.{$msgTables['Message']} AS Message, anatoa_{$msgTables['tblName']}.{$msgTables['Date']} AS Date, '{$msgTables['tblName']}' AS tableName
							FROM anatoa_{$msgTables['tblName']}
						  WHERE anatoa_{$msgTables['tblName']}.{$msgTables['Sender']} = $profileId
						  ORDER BY anatoa_{$msgTables['tblName']}.{$msgTables['Date']} DESC;";

			 	$r = array();
		      	$result = db_query( $query );
		      	while( $r = db_fetch_array( $result ) )
			 		$msgArray[] =  $r;
	 		}
		}
		return $msgArray;
	}

	function getImagesOfDeletedProfile($profileId)
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
		$imageArray = array();

		if(!empty($anatoaConfig->Tables['Images']))
		{
	 		foreach ($anatoaConfig->Tables['Images'] as $imgTables)
	 		{
	 			$arrFields = array('priKey','MemberId','Filename','Title','DateCreated');
	 			$strFields = '';

	 			foreach ( $arrFields as $fields)
				{
					if(($imgTables[$fields] == 'nid' || $imgTables[$fields] == 'uid') && $imgTables['tblName']=='node') $strFields .= 'node.';
	 				$strFields .= "{$imgTables[$fields]} AS $fields,";
				}

	 			$query = "SELECT $strFields '{$imgTables['tblName']}' AS tableName
								FROM `anatoa_{$imgTables['tblName']}` as  {$imgTables['tblName']} ";

				if($imgTables['JOIN']!='')
		      		$query .= $imgTables['JOIN'];

		      	$query .= " WHERE {$imgTables['tblName']}.{$imgTables['MemberId']} = $profileId";

		      	$r = array();
		      	$result = db_query( $query );
		      	while( $r = db_fetch_array( $result ) )
			 		$imageArray[] =  $r;
	 		}
		}
 		return $imageArray;
	}

	function getAddressCountry($profileId)
	{
 		return db_result(db_query("SELECT country FROM addresses_user WHERE eid = $profileId"));
	}

	function deleteAllUserRelatedData($profileId)
	{
		db_query("DELETE FROM anatoa_transactionlog WHERE MemberId = $profileId");
		db_query("DELETE FROM anatoa_mst_profiles WHERE ProfileID = $profileId");
		db_query("DELETE FROM anatoa_mst_messages WHERE ProfileID = $profileId");
		db_query("DELETE FROM anatoa_mst_images WHERE ProfileID = $profileId");
	}

}