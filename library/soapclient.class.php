<?php
/* Anatoa Drupal Plugin
*  Copyright 2009 Univalence Ltd
*  All rights reserved
*
* DISCLAIMER
* Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
* (whether such damage or losses were foreseen, foreseeable, known or otherwise):
* (a) loss of data;
* (b) loss of revenue or anticipated profits;
* (c) loss of business;
* (d) loss of opportunity;
* (e) loss of goodwill or injury to reputation;
* (f) losses suffered by third parties; or
* (g) any indirect, consequential, special or exemplary damages
* arising from the use of Anatoa.com or associated web services regardless of the
* form of action, including the use of this source code.
*/

require_once(drupal_get_path('module', 'anatoa') .'/library/nusoap.php');
require_once(drupal_get_path('module', 'anatoa') .'/library/class.wsdlcache.php');
require_once(drupal_get_path('module', 'anatoa') .'/config/config.class.php');

class anatoaSoapClient
{
	private $client;
	private $cache;
	private $wsdl;

	function __construct()
	{
	}

	// Init returns true on success
	function init()
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

		$fileDir = dirname(__FILE__);
		$fileDir = substr($fileDir,0,-1-strlen('library'));
		// first look for WSDL in local cache
		$this->cache = new nusoap_wsdlcache($fileDir.$anatoaConfig->tempDir, 86400);
		$this->wsdl = $this->cache->get($anatoaConfig->soapURL);
		if(is_null($this->wsdl))
		{
			// cache not found or expired; load WSDL from Anatoa
			$this->wsdl = new wsdl($anatoaConfig->soapURL, '', '', '', '', 5);
			$this->cache->put($this->wsdl);
		}

		// set up soap client
		$this->client = new nusoap_client($this->wsdl,'wsdl','','','','');

		$err = $this->client->getError();
		if($err)
		{
			error_log("\nFailed creation nusoap_client: ".$err,0);
			return false;
		}

		$this->client->setDebugLevel(0);

		return true;
	}

	// callMethod returns true on success
	function callMethod( $APIKey, $method, $paramName, $paramValue, &$returnValue )
	{
		$anatoaConfig = anatoaConfiguration::getInstance();

		if(empty($this->client))
		{
			error_log("Attempted to call uninitialized anatoaSoapClient",0);
			return false;
		}

		$parametersPassed =
			array( 'ClientInfo'=>array(	'APIKey'=>$APIKey,
										'Name'=>$anatoaConfig->pluginName,
										'Version'=>$anatoaConfig->pluginVersion ) );
		if( $method != 'verifyKey' )
			$parametersPassed[$paramName] = $paramValue;

		$returnValue = $this->client->call($method,$parametersPassed);

		if($this->client->fault)
		{
			error_log("Fault calling webservice method ".$method.": ".print_r($returnValue,true),0);
			return false;
		}
		else
		{
			$err = $this->client->getError();
			if($err)
			{
				error_log("Error calling webservice method ".$method.": ".$err,0);
				return false;
			}
			else
				return true;
		}
	}

}