<?php
/* Anatoa Drupal Plugin
*  Copyright 2009 Univalence Ltd
*  All rights reserved
*
* DISCLAIMER
* Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
* (whether such damage or losses were foreseen, foreseeable, known or otherwise):
* (a) loss of data;
* (b) loss of revenue or anticipated profits;
* (c) loss of business;
* (d) loss of opportunity;
* (e) loss of goodwill or injury to reputation;
* (f) losses suffered by third parties; or
* (g) any indirect, consequential, special or exemplary damages
* arising from the use of Anatoa.com or associated web services regardless of the
* form of action, including the use of this source code.
*/

class anatoaUtility
{
	/**
	 * Validates give IP Address
	 *
	 * @param string $ip_addr
	 * @return boolean(true or false)
	 */
	static function validateIpAddress($ip_addr)
	{
	  //first of all the format of the ip address is matched
	  if(preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/",$ip_addr))
	  {
	    //now all the intger values are separated
	    $parts=explode(".",$ip_addr);
	    //now we need to check each part can range from 0-255
	    foreach($parts as $ip_parts)
	    {
	      if(intval($ip_parts)>255 || intval($ip_parts)<0)
	      return false; //if number is not within range of 0-255
	    }
	    return true;
	  }
	  else
	    return false; //if format of ip address doesn't match
	}

	/**
	 * Get IP Address and Proxy IP Address of user
	 *
	 * @return array containing IPAddress and ProxyIP
	 */
	static function getIPAddress()
	{
		$ipAddress ='';
		$proxy = '';
		$forwarded = $_SERVER["HTTP_X_FORWARDED_FOR"];
	    if($forwarded)
	           {
	               $forwarded = preg_split("/,| /",$forwarded);
	               $forwarded = $forwarded[0];
	                 // ignore private IP ranges
				   if (preg_match("/^((10|127|172\.(1[6-9]|2[0-9]|3[0-1])|192\.168)\.|169\.254\.)/",$forwarded))
	                        $forwarded = "";
		           if (!preg_match("/^([0-9]{1,3}(\.|$)){4}/",$forwarded) && $forwarded)
	                        $forwarded = "";
	               if($forwarded)
	               {
	               		$ipAddress = $forwarded;
					    $proxy = $_SERVER["REMOTE_ADDR"];
	               }
	           }
		// if not through proxy then default to remote device address
	    if($forwarded=="")
			     $ipAddress = $_SERVER["REMOTE_ADDR"];

		return array('IPAddress'=>$ipAddress,'ProxyIP'=>$proxy);
	}
}