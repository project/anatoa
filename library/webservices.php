<?php
/* Anatoa Drupal Plugin v1.0
*  Copyright 2009 Univalence Ltd
*  All rights reserved
*
* DISCLAIMER
* Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
* (whether such damage or losses were foreseen, foreseeable, known or otherwise):
* (a) loss of data;
* (b) loss of revenue or anticipated profits;
* (c) loss of business;
* (d) loss of opportunity;
* (e) loss of goodwill or injury to reputation;
* (f) losses suffered by third parties; or
* (g) any indirect, consequential, special or exemplary damages
* arising from the use of Anatoa.com or associated web services regardless of the
* form of action, including the use of this source code.
*/

class anatoaWebService
{

	function __construct()
	{}

	static function checkProfileOnInsertOrUpdate()
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
  		global $user;

		$objDBModel = new dbModel();
		$fields = $objDBModel->getFieldList();

		$firstName = '';
		$lastName = '';
		$country = '';

		$firstNameFieldID = variable_get('anatoa_FirstName',0);
		if($firstNameFieldID != 0)
		 	$firstNameField = $fields[2][$firstNameFieldID];

		$lastNameFieldID = variable_get('anatoa_LastName',0);
		if($lastNameFieldID != 0)
		 	$lastNameField = $fields[2][$lastNameFieldID];

		$countryFieldID = variable_get('anatoa_Country',0);
		if($countryFieldID != 0)
		 	$countryField = $fields[2][$countryFieldID];

		$customFieldsData = $objDBModel->getCustomProfileFieldData($user->uid, $firstNameFieldID.','.$lastNameFieldID.','. $countryFieldID);

		$userCountry = $anatoaConfig->reqMod['anatoa_addresses_user'] ? $user->addresses['country'] : $customFieldsData[$countryFieldID];

		$IP = anatoaUtility::getIPAddress();
		$query = "SELECT COUNT(*) FROM {$anatoaConfig->MasterTables['Profile']} WHERE ProfileID = {$user->uid} AND TableName = '".$anatoaConfig->ProfileTable['Drupal']['tblName']."'";

    	if(db_result(db_query($query)) == 0)
    	 	{
	   		  	$query ='INSERT INTO '.$anatoaConfig->MasterTables['Profile']." (ProfileID,UserName,Email,TableName,FirstName,LastName,Country,IPAddress,ProxyIP) VALUES
	   		 		  ({$user->uid},'{$user->name}','{$user->mail}','".$anatoaConfig->ProfileTable['Drupal']['tblName']."','$customFieldsData[$firstNameFieldID]',
	   		 		  '$customFieldsData[$lastNameFieldID]', '$userCountry','{$IP['IPAddress']}','{$IP['ProxyIP']}')";
	    	  	db_query($query);
    	 	}
    	else
    	 	{
    	 	 	$query ="UPDATE {$anatoaConfig->MasterTables['Profile']} SET
    	 					UserName = '$user->name',
							Email = '$user->mail',
							FirstName = '$customFieldsData[$firstNameFieldID]',
							LastName = '$customFieldsData[$lastNameFieldID]',
							Country = '$userCountry',
							TableName = '".$anatoaConfig->ProfileTable['Drupal']['tblName']."',
							IPAddress ='{$IP['IPAddress']}',
							ProxyIP = '{$IP['ProxyIP']}'
   		 			WHERE ProfileID = {$user->uid} AND TableName = '".$anatoaConfig->ProfileTable['Drupal']['tblName']."'";
    	 	 	db_query($query);
    		}

		if(db_affected_rows() > 0 && $user->created > variable_get('anatoa_installationDate',0) )
		{
		  $soapclient = new anatoaSoapClient();
		  if($soapclient->init())
			{
				$resultSet = $objDBModel->getAPIConfig();
				$APIKey = $resultSet['APIKey'];
				$rejection_threshold = $resultSet['Rejection_Threshold'];
				$action = $resultSet['Action'];
				$full_email = $resultSet['Check_full_email'];
				$keyCheck = $resultSet['keyChecked'];

				// =============================== Profile Check =====================================
				if($full_email == 'Yes')
				{
					$emailAddressHash = '';
					$emailAddress = $user->mail;
				}
				else
				{
					$emailAddressHash = md5($user->mail);
					$emailAddress = '';
				}

				$profileIPAddress = anatoaUtility::validateIpAddress($IP['IPAddress']) ? $IP['IPAddress'] : '';
				$profileProxyIPAddress = anatoaUtility::validateIpAddress($IP['ProxyIP']) ? $IP['ProxyIP'] : '';

				$profileParams = array(	'UserName'=>$user->name,
										'EmailHash'=>$emailAddressHash,
										'Email'=>$emailAddress,
										'IPAddress'=>$profileIPAddress,
										'ProxyIP'=>$profileProxyIPAddress );

				if(!empty($customFieldsData[$firstNameFieldID]))
					$profileParams['FirstName'] = $customFieldsData[$firstNameFieldID];

				if(!empty($customFieldsData[$lastNameFieldID]))
					$profileParams['LastName'] = $customFieldsData[$lastNameFieldID];

				if( $anatoaConfig->reqMod['anatoa_addresses_user'] && !empty($user->addresses['country']))
					$profileParams['Country'] = strtoupper($user->addresses['country']);
				elseif(!empty($customFieldsData[$countryFieldID]))
					$profileParams['Country'] = $customFieldsData[$countryFieldID];

				if($soapclient->callMethod( $APIKey, 'checkProfile', 'Profiles', array( 'Profile'=>$profileParams ), $returnValues ) )
				{
					$return = $returnValues[0];
					if(is_numeric($return))
					{
						$fraudulent = 0;
						if($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
							$fraudulent = 1;

						$extra = !empty($IP['IPAddress']) ? 'WithIP' : '';
						$objDBModel->updateWithSoapCheckResult($user->uid, $anatoaConfig->ProfileTable['Drupal']['priKey'], $user->uid,'users','Profile',$return,$fraudulent,$action,$extra);
						$objDBModel->enterLog('Check Profile','Manul:Admin',$user->uid,$user->uid,'users', addslashes(serialize($profileParams)),$return);
						variable_set('CheckProfile_limit_Reached',0);

						if($fraudulent)
						{
							module_invoke_all('anatoa', 'fradulent_profile_found', $user);
						}
					}
					else if($return == 'USAGE_COMPLETE')
					{
						variable_set('CheckProfile_limit_Reached',1);
					}
				}
			}
		}
	}

	/**
	* This function will report a given profile, together with any profile images to Anatoa using SOAP.
	*  It will log each SOAP call.
	*/
	function reportProfile($profileID,$reasonForReporting='Reported By Site Admin',$mode='Block',$calledBy = 'Profile Listing Page',$rptDetail=':')
	{
		$objDBModel = new dbModel();
		$anatoaConfig = anatoaConfiguration::getInstance();

		$reportForDetail = explode(':',$rptDetail);

		$resultSet = $objDBModel->getAPIConfig();
		$APIKey = $resultSet['APIKey'];
		$rejection_threshold = $resultSet['Rejection_Threshold'];
		$action = $resultSet['Action'];
		$full_email = $resultSet['Check_full_email'];
		$keyCheck = $resultSet['keyChecked'];

		if(empty($APIKey) || $keyCheck != 'Valid')
			return "Error:APIKey";
		$soapclient = new anatoaSoapClient();
		if(!$soapclient->init())
			return  "Error:Connection";

		// =============================== Report Profile =====================================

		$Profile = $objDBModel->getProfileDetails($profileID);
		if(empty($Profile['mail'])) $Profile['mail'] = $Profile['altEmail'];

		if($Profile['status'] == 'Block' && $mode == 'Block' )
			return '';

		$firstNameFieldID = variable_get('anatoa_FirstName',0);
		$lastNameFieldID = variable_get('anatoa_LastName',0);
		$countryFieldID = variable_get('anatoa_Country',0);

		$customFieldsData = $objDBModel->getCustomProfileFieldData($profileID, $firstNameFieldID.','. $lastNameFieldID .','. $countryFieldID);
		$customFieldsData[0] = '';

		$profileParams = array(	'UserName'=>$Profile[$anatoaConfig->ProfileTable['Drupal']['UserName']],
									'Email'=>$Profile[$anatoaConfig->ProfileTable['Drupal']['Email']],
									'IPAddress'=>$Profile['IPAddress'],
									'Country'=>$Profile['Country'],
									'ProxyIP'=>$Profile['ProxyIP'],
									'Reason'=> $reasonForReporting );

		if(!empty($customFieldsData[$firstNameFieldID]))
					$profileParams['FirstName'] = $customFieldsData[$firstNameFieldID];

		if(!empty($customFieldsData[$lastNameFieldID]))
					$profileParams['LastName'] = $customFieldsData[$lastNameFieldID];

		if( $anatoaConfig->reqMod['anatoa_addresses_user'] )
		{
			$userCountry = $objDBModel->getAddressCountry($profileID);
		    if(!empty($userCountry))
				$profileParams['Country'] = strtoupper($userCountry);
		}
		elseif(!empty($customFieldsData[$countryFieldID]))
			$profileParams['Country'] = $customFieldsData[$countryFieldID];

		if( !$soapclient->callMethod( $APIKey, 'reportProfile', 'Profiles', array( 'Profile'=>$profileParams ), $returnValues ) )
				return  "Error:Connection";

		$return = $returnValues[0];
		$profileParams['APIKey'] = $APIKey;

		$objDBModel->updateWithSoapReportResult($profileID, $anatoaConfig->ProfileTable['Anatoa']['priKey'], $profileID,$anatoaConfig->ProfileTable['Drupal']['tblName'],'Profile',$return,$reasonForReporting,'',$mode,$calledBy,$rptDetail);
		$objDBModel->enterLog('Report Profile',"Manul:Admin ($calledBy)",$profileID, $profileID,'users', serialize($profileParams),$return,$reasonForReporting);


	//================================================= Report Images ========================================================

		$Images = $objDBModel->getAllImagesOfProfile($profileID,'Report');

		foreach ($Images as $image)
		{
			$fileDir = dirname(__FILE__);
			$fileDir = str_replace(array('sites/all/modules/anatoa/library','sites\\all\\modules\\anatoa\\library'),'',$fileDir);

			$i = $image['tblIndex'];
			$file = $fileDir.$image[$anatoaConfig->Tables['Images'][$i]['Filename']];
			$id = $image[$anatoaConfig->Tables['Images'][$i]['priKey']];
			$idField = $anatoaConfig->Tables['Images'][$i]['priKey'];

			if (file_exists($file) && !empty($image[$anatoaConfig->Tables['Images'][$i]['Filename']]))
			{
				$f1 = fopen($file,'r');
				$content = fread($f1,filesize($file));
				fclose($f1);

				$imageHash = md5($content);
				$content = base64_encode($content);

				$imageParams = array(	'Image'=>$content,
										'Email'=>$Profile['mail'] );

				if( !$soapclient->callMethod( $APIKey, 'reportImage', 'Images', array( 'Image'=>$imageParams ), $returnValues ) )
					return  "Error:Connection";

				$return = $returnValues[0];

				$forLogParameters = array('APIKey'=>$APIKey,'ImageHash'=>$imageHash,'Email'=>$Profile['mail']);

				$exceptions = ($rptDetail=':' || empty($rptDetail)) ? ':' : $id.':'.$image['tableName'] ;
				$objDBModel->updateWithSoapReportResult($profileID,$idField,$id,$image['tableName'],'Image',$return,$reasonForReporting,$image[$anatoaConfig->Tables['Images'][$i]['Filename']],$mode,$calledBy,$exceptions);
				$objDBModel->enterLog('Report Image',"Manul:Admin ($calledBy)",$profileID,$id,$image['tableName'],serialize($forLogParameters),$return,$reasonForReporting);
			}
		}
	}

	function ajaxCheckProfile($profileID)
	{
		$objDBModel = new dbModel();
		$soapclient = new anatoaSoapClient();
		$anatoaConfig = anatoaConfiguration::getInstance();

		$fp = 0;
		$fi = 0;
		$fm = 0;
		$fraudulent = 0;
		$profileReported = 0;

		$resultSet = $objDBModel->getAPIConfig();
		$APIKey = $resultSet['APIKey'];
		$rejection_threshold = $resultSet['Rejection_Threshold'];
		$action = $resultSet['Action'];
		$full_email = $resultSet['Check_full_email'];
		$keyCheck = $resultSet['keyChecked'];

		if(empty($APIKey) || $keyCheck != 'Valid')
			return "Error:APIKey";

		if(!$soapclient->init())
			return "Error:Connection";

		$userObj = $userObj = db_fetch_object(db_query('SELECT * FROM {users} WHERE uid = %d',$profileID));

		// =============================== Profile Check =======================================

		$Profile = $objDBModel->getProfileDetails($profileID);

		if($full_email == 'Yes')
		{
			$emailAddressHash = '';
			$emailAddress = $Profile['mail'];
		}
		else
		{
			$emailAddressHash = md5($Profile['mail']);
			$emailAddress = '';
		}

		$firstNameFieldID = variable_get('anatoa_FirstName',0);
		$lastNameFieldID = variable_get('anatoa_LastName',0);
		$countryFieldID = variable_get('anatoa_Country',0);

		$customFieldsData = $objDBModel->getCustomProfileFieldData($profileID,$firstNameFieldID.','.$lastNameFieldID.','.$countryFieldID);
		$customFieldsData[0] = '';

		$profileParams = array(	'UserName'=>$Profile[$anatoaConfig->ProfileTable['Drupal']['UserName']],
									'Email'=>$Profile[$anatoaConfig->ProfileTable['Drupal']['Email']],
									'IPAddress'=>$Profile['IPAddress'],
									'Country'=>$Profile['Country'],
									'ProxyIP'=>$Profile['ProxyIP'],
									'Reason'=> $reasonForReporting );

		if(!empty($customFieldsData[$firstNameFieldID]))
					$profileParams['FirstName'] = $customFieldsData[$firstNameFieldID];

		if(!empty($customFieldsData[$lastNameFieldID]))
					$profileParams['LastName'] = $customFieldsData[$lastNameFieldID];

		if( $anatoaConfig->reqMod['anatoa_addresses_user'] )
		{
			$userCountry = $objDBModel->getAddressCountry($profileID);
		    if(!empty($userCountry))
				$profileParams['Country'] = strtoupper($userCountry);
		}
		elseif(!empty($customFieldsData[$countryFieldID]))
			$profileParams['Country'] = $customFieldsData[$countryFieldID];

		if( !$soapclient->callMethod( $APIKey, 'checkProfile', 'Profiles', array( 'Profile'=>$profileParams ), $returnValues ) )
				return  "Error:Connection";

		$return = $returnValues[0];
		$profileParams['APIKey'] = $APIKey;

		if(!is_numeric($return))
		{
			if($return == 'USAGE_COMPLETE')
				return "Error:Processing your request.<br> Server Response : ".str_replace('{--ServiceType--}','Profile',$anatoaConfig->soapResponse['USAGE_COMPLETE']);

			$query = "SELECT COUNT(*) FROM {$anatoaConfig->MasterTables['Profile']} WHERE ProfileID = {$profileID} AND TableName = '".$anatoaConfig->ProfileTable['Drupal']['tblName']."'";

    	 	if(db_result(db_query($query)) == 0)
    	 	{
	   		  	$query ='INSERT INTO '.$anatoaConfig->MasterTables['Profile']." (ProfileID,UserName,Email,TableName,CheckStatus,checkReasons,checkDate,checkMode) VALUES
	   		 		  ($profileID,'{$Profile[$anatoaConfig->ProfileTable['Drupal']['UserName']]}','{$Profile[$anatoaConfig->ProfileTable['Drupal']['Email']]}','".$anatoaConfig->ProfileTable['Drupal']['tblName']."','Failed Check','$return' ,".time().",'Check Old Profile')";
	    	  	db_query($query);
    	 	}
    	 	else
    	 	{
    	 	 	$query ="UPDATE {$anatoaConfig->MasterTables['Profile']} SET
    	 					CheckStatus = 'Failed Check',checkReasons = '$return' ,checkDate = ".time().",checkMode='Check Old Profile'
   		 			WHERE ProfileID = {$profileID} AND TableName = '".$anatoaConfig->ProfileTable['Drupal']['tblName']."'";
    	 	 	db_query($query);
    		}

			$objDBModel->enterLog('Check Profile','Check Old Profile',$profileID,$profileID,$anatoaConfig->ProfileTable['Drupal']['tblName'],addslashes(serialize($profileParams)),$return);
		}
		elseif($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
		{
			$fp++;
			$fraudulent =1;
		}

		if($return > 6.0)
		{
			$contents = '';
			$contents = $this->reportProfile($profileID,'Profile found to be fraudulent',$action,'Check Old Profiles',$profileID.':'.$anatoaConfig->ProfileTable['Drupal']['tblName']);

			if($contents == 'Error:Connection' || $contents == 'Error:APIKey' || $contents == 'Error:Processing' )
				return $contents;

			$profileReported = 1;
		}

		if(is_numeric($return))
		{
			$extra = !empty($Profile['IPAddress']) ? 'WithIP' : '';
			$objDBModel->updateWithSoapCheckResult($profileID,$anatoaConfig->ProfileTable['Drupal']['priKey'],$profileID,$anatoaConfig->ProfileTable['Drupal']['tblName'],'Profile',$return,$fraudulent,$action,$extra);
			$objDBModel->enterLog('Check Profile','Check Old Profile',$profileID,$profileID,$anatoaConfig->ProfileTable['Drupal']['tblName'],addslashes(serialize($profileParams)),$return);
		}

		if($fraudulent)
		{
			module_invoke_all('anatoa', 'fradulent_profile_found', $userObj);
		}

		if($profileReported)
		  	return "1;1";

	//=================================== Image Check ======================================

		$Images = $objDBModel->getAllImagesOfProfile($profileID,'Report');

		foreach ($Images as $image)
		{
			$fileDir = dirname(__FILE__);
			$fileDir = str_replace(array('sites/all/modules/anatoa/library','sites\\all\\modules\\anatoa\\library'),'',$fileDir);

			$i = $image['tblIndex'];
			$file = $fileDir.$image[$anatoaConfig->Tables['Images'][$i]['Filename']];
			$id = $image[$anatoaConfig->Tables['Images'][$i]['priKey']];
			$idField = $anatoaConfig->Tables['Images'][$i]['priKey'];

			if (file_exists($file) && !empty($image[$anatoaConfig->Tables['Images'][$i]['Filename']]))
			{
				$f1 = fopen($file,'r');
				$content = fread($f1,filesize($file));
				fclose($f1);

				$imageHash = md5($content);
				$imageParams = array('ImageHash'=>$imageHash );
				if( !$soapclient->callMethod( $APIKey, 'checkImage', 'Images', array( 'Image'=>$imageParams ), $returnValues ) )
					return  "Error:Connection";

				$return = $returnValues[0];
				$fraudulent = 0;
				$imageParams['APIKey'] = $APIKey;

				if(!is_numeric($return))
				{
					if($return == 'USAGE_COMPLETE')
						return "Error:Processing your request.<br> Server Response : ".str_replace('{--ServiceType--}','Image',$anatoaConfig->soapResponse['USAGE_COMPLETE']);

					$query = "SELECT COUNT(*) FROM {$anatoaConfig->MasterTables['Image']} WHERE ProfileID = {$profileID} AND ImageID=$id AND TableName = '".$image['tableName']."'";

		    	 	if(db_result(db_query($query)) == 0)
		    	 	{
			   		  	$query ='INSERT INTO '.$anatoaConfig->MasterTables['Image']." (ProfileID,ImageID,TableName) VALUES ($profileID,$id,'".$image['tableName']."')";
			    	  	db_query($query);
		    	 	}
		    	 	else
		    	 	{
		    	 	 	$query ="UPDATE {$anatoaConfig->MasterTables['Image']} SET
		    	 					CheckStatus = 'Failed Check',checkReasons = '$return' ,checkDate = ".time().",checkMode='Check Old Profile'
		   		 			WHERE ProfileID = {$profileID} AND ImageID=$id AND TableName = '".$image['tableName']."'";
		    	 	 	db_query($query);
		    		}

					$objDBModel->enterLog('Check Image','Check Old Profile',$profileID,$id,$image['tableName'],addslashes(serialize($imageParams)),$return);

					continue;
				}
				elseif($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
				{
					$fi++;
					$fraudulent =1;
				}

				if($return > 6.0)
				{
					$contents = '';
					$contents = $this->reportProfile($profileID,'Image found to be fraudulent', $action,'Check Old Profiles', $id.':'.$image['tableName']);

					if($contents == 'Error:Connection' || $contents == 'Error:APIKey' || $contents == 'Error:Processing' )
						return $contents;

					$profileReported = 1;
				}

				$objDBModel->updateWithSoapCheckResult($profileID,$idField,$id,$image['tableName'],'Image',$return,$fraudulent,$action,$image[$anatoaConfig->Tables['Images'][$i]['Filename']]);
				$objDBModel->enterLog('Check Image','Check Old Profile',$profileID, $id, $image['tableName'], addslashes(serialize($imageParams)), $return);

				if($fraudulent)
				{
					module_invoke_all('anatoa', 'fradulent_image_found', $userObj);
				}

				if($profileReported)
		  			return "1;1";
			}
		}

		//====================================================   Message Check  ===================================================

		$Messages = $objDBModel->getAllMessagesOfProfile($profileID);
		foreach ($Messages as $messgs)
		{
			$msgData = $messgs[ $anatoaConfig->Tables['Message'][$messgs['tblIndex']]['Message'] ];
		    $messageParams = array( 'Message'=>strip_tags(stripslashes($msgData)) );

			if( !$soapclient->callMethod( $APIKey, 'checkMessage', 'Messages', array( 'Message'=>$messageParams ), $returnValues ) )
				return  "Error:Connection";

			$i = $messgs['tblIndex'];
			$id = $messgs[$anatoaConfig->Tables['Message'][$i]['priKey']];
			$return = $returnValues[0];
			$fraudulent = 0;
			$messageParams['APIKey'] = $APIKey;

			if(!is_numeric($return))
			{
				if($return == 'USAGE_COMPLETE')
						return "Error:Processing your request.<br> Server Response : ".str_replace('{--ServiceType--}','Message',$anatoaConfig->soapResponse['USAGE_COMPLETE']);
				elseif($return == 'MESSAGE_REQUIRED')
					$return = 0;

				if($return!=0)
				{ 
	   		  		$query = "SELECT COUNT(*) FROM {$anatoaConfig->MasterTables['Message']} WHERE ProfileID = {$profileID} AND MessageID=$id AND TableName = '".$messgs['tableName']."'";

		    	 	if(db_result(db_query($query)) == 0)
		    	 	{
			   		  	$query ='INSERT INTO '.$anatoaConfig->MasterTables['Message']." (ProfileID,MessageID,TableName) VALUES ($profileID,$id,'".$messgs['tableName']."')";
			    	  	db_query($query);
		    	 	}
		    	 	else
		    	 	{
		    	 	 	$query ="UPDATE {$anatoaConfig->MasterTables['Message']} SET
		    	 					CheckStatus = 'Failed Check',checkReasons = '$return' ,checkDate = ".time().",checkMode='Check Old Profile'
		   		 			WHERE ProfileID = {$profileID} AND MessageID=$id AND TableName = '".$messgs['tableName']."'";
		    	 	 	db_query($query);
		    		}

					$objDBModel->enterLog('Check Message','Check Old Profile',$profileID,$id,$messgs['tableName'],addslashes(serialize($messageParams)),$return);

					continue;
				}
			}
			elseif($return > $anatoaConfig->rejectionThreshold[$rejection_threshold])
				{
					$fi++;
					$fraudulent =1;
				}

			if($return > 6.0)
			{
				$contents = '';
				$contents = $this->reportProfile($profileID,'Message found to be fraudulent',$action,'Check Old Profiles',$id.':'.$messgs['tableName']);

				if($contents == 'Error:Connection' || $contents == 'Error:APIKey' || $contents == 'Error:Processing' )
						return $contents;

				$profileReported = 1;
			}

			$objDBModel->updateWithSoapCheckResult($profileID,$anatoaConfig->Tables['Message'][$i]['priKey'],$id,$messgs['tableName'],'Message',$return,$fraudulent,$action);
			$objDBModel->enterLog('Check Message','Check Old Profile',$profileID,$id,$messgs['tableName'],addslashes(serialize($messageParams)),$return);

			if($fraudulent)
				{
					module_invoke_all('anatoa', 'fradulent_message_found', $userObj);
				}

			if($profileReported)
		  			return "1;1";
		}

		return "1;$fp";
	}


}