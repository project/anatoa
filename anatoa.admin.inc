<?php
/* Anatoa Drupal Plugin v1.0
*  Copyright 2009 Univalence Ltd
*  All rights reserved
*
* DISCLAIMER
* Anatoa or Univalence Ltd shall not be liable for any of the following losses or damage
* (whether such damage or losses were foreseen, foreseeable, known or otherwise):
* (a) loss of data;
* (b) loss of revenue or anticipated profits;
* (c) loss of business;
* (d) loss of opportunity;
* (e) loss of goodwill or injury to reputation;
* (f) losses suffered by third parties; or
* (g) any indirect, consequential, special or exemplary damages
* arising from the use of Anatoa.com or associated web services regardless of the
* form of action, including the use of this source code.
*/
/**
 * @file
 * Administration page callbacks for the anatoa module.
 */

$numOfProfileToCheck=0;

function anatoa_admin_config()
{
	$output = t('A (free) account at <a href=\'@anatoaUrl\' target=\'_blank\'>anatoa.com</a> is required to use the Anatoa fraud detection plugin. Please go to anatoa.com, sign up for an account, and enter your assigned API key below.',array('@anatoaUrl'=>url('http://anatoa.com')));

	$output .= '<br><h3>'.t('Configuration').'</h3>';
	$output .= drupal_get_form('anatoa_configurationForm');
	return $output;
}

/**
 * Define a form.
 */
function anatoa_configurationForm(&$form_state = NULL) {

	$objDBModel = new dbModel();

	$apiConfig = $objDBModel->getAPIConfig();
	$apiKey = isset($_POST['APIKey'])? $_POST['APIKey'] : $apiConfig['APIKey'] ;
	$rejectionThreshHold = isset($_POST['rejection_threshold']) ? $_POST['rejection_threshold'] : $apiConfig['Rejection_Threshold'] ;
	//$action = isset($_POST['action']) ? $_POST['action'] : $apiConfig['Action'] ;
	$fullEmail = isset($_POST['full_email']) ? $_POST['full_email'] : $apiConfig['Check_full_email'] ;

	$form['#cache'] = FALSE;
	$form['apikey'] = array(
		'#title' => t('API Key'),
		'#type' => 'fieldset'
		);
	$form['apikey']['APIKey'] = array(
		'#title' => t('API Key'),
		'#type' => 'textfield',
		'#default_value'=>$apiKey,
		'#description' => t('Please enter your API key for anatoa plugin.')
		);
	$form['apikey']['testAPI'] = array(
		'#type' => 'submit',
		'#name'=>'testApiKey',
		'#value' => t('Test API Key')
		);
	$form['advSetting'] = array(
		'#title' => t('Advanced Settings'),
		'#type' => 'fieldset'
		);
	$form['rejectionThrash'] = array(
		'#type' => 'value',
		'#value' => array('Low'=>t('Low'),'Standard'=>t('Standard'),'High'=>t('High'),'Disabled'=>t('Disabled'))
		);
	$form['emailaction'] = array(
		'#type' => 'value',
		'#value' => array('Yes'=>t('Yes'),'No'=>t('No'))
		);
	$form['advSetting']['rejection_threshold'] = array(
		'#title' => t('Profile Rejection Threshold'),
		'#type' => 'select',
		'#default_value' =>  $rejectionThreshHold,
		'#description' => t('<b>Low :</b> only the most obvious fraudulent profiles are rejected. <br> <b>Standard : </b> recommended setting.<br><b>High :</b> any small indication of possible fraud could cause rejection. This setting could cause \'false positives\'. e.g. rejection of legitimate profiles.<br> <b>Disabled :</b> do not reject any profiles.'),
		'#options' => $form['rejectionThrash']['#value']
		);
	$form['advSetting']['action'] = array(
		'#title' => t('Action'),
		'#type' => 'item',
		'#default_value' => 'Block',
		'#description' => t('<b>Block profile :</b> profile will be blocked if found to be fraudulent, for manual review.'),
		);
	$form['advSetting']['full_email'] = array(
		'#title' => t('Check full email address'),
		'#type' => 'select',
		'#description' => t('<b>Yes :</b> The full email address will be checked against Anatoa. <br> <b>No :</b> A one way hash value is used instead which means Anatoa will never see the actual email address. A setting of \'Yes\' is recommended as it helps the detection process.'),
		'#options' => $form['emailaction']['#value'],
		'#default_value' => $fullEmail
		);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
		);
	$form['cancel'] = array(
		'#type' => 'button',
		'#value' => t('Cancel')
		);
	if (isset($form_state['values']['APIKey'])) {
		$form['apikey']['APIKey']['#value'] = $form_state['values']['APIKey'];
	}

	return $form;
}

/**
 * Validate the configuration form.
 */
function anatoa_configurationForm_validate($form, &$form_state) {
	if ($form_state['values']['APIKey'] == '') {
		// We notify the form API that this field has failed validation.
		form_set_error('APIKey',t('API key cannot be blank.'));
	}
	else
	{
		$anatoaConfig = anatoaConfiguration::getInstance();
		$soapclient = new anatoaSoapClient();

		if(!$soapclient->init())
			form_set_error('APIKey',t($anatoaConfig->genMsg['noResponse']));
		else
		{
			$soapclient->callMethod( trim($form_state['values']['APIKey']), 'verifyKey', '', '', $return );

			if($return === false)
			 	form_set_error('APIKey',t($anatoaConfig->genMsg['ConnectionError']));
			elseif($return == 'VALID_API_KEY')
				drupal_set_message(t($anatoaConfig->soapResponse[$return]));
			elseif(!empty($return) && !empty($anatoaConfig->soapResponse[$return]))
				form_set_error('APIKey',t($anatoaConfig->soapResponse[$return]));
			elseif(!empty($return))
			  	form_set_error('APIKey',print_r($return,true));
			else
				form_set_error('APIKey',t($anatoaConfig->genMsg['noResponse']));
		}
	   form_set_value($form['apikey']['APIKey'], $form_state['values']['APIKey'], $form_state);
	}

}

/**
 * Handle post-validation form submission.
 */
function anatoa_configurationForm_submit($form, &$form_state)
{
	db_query("UPDATE {anatoa_config} SET APIKey ='%s', Rejection_Threshold ='%s', Action='%s', Check_full_email='%s' ,keyChecked='%s', ModifiedBy='1' ,LastModified=".time()." LIMIT 1;",
		$form_state['values']['APIKey'],
		$form_state['values']['rejection_threshold'],
		'Block',  //$form_state['values']['action']
		$form_state['values']['full_email'],
		'Valid'
		);
	drupal_set_message(t('Anatoa plugin configured successfully'));
}

function anatoa_admin_mapfields()
{
	$output = '';
	if(variable_get('anatoa_FirstName',0) == 0 && variable_get('anatoa_LastName',0)==0 && variable_get('anatoa_Country',0) == 0)
		$output .=  '<div class="message error">'.t('First name, Last name or country field mapped is not saved, please save after properly mapping the fields.').'</div><br>';

	$output .= t('For proper working of this plugin you need to map profile fields with anatoa\'s plugin table. Please map the fields carefully and correctly as this is going to effect anatoa\'s efficiency in fraud detecting.');

	$output .= '<br><h3>'.t('Map Profile Fields').'</h3>';
	$output .= drupal_get_form('anatoa_mapfieldsForm');
	return $output;
}

function anatoa_mapfieldsForm(&$form_state)
{
	$objDBModel = new dbModel();
	$fields = $objDBModel->getFieldList();
	$fields[0] = empty($fields[0]) ? array() : $fields[0];
	$defaultValues = $fields[1];

	$fieldList = array_merge(array('Select'=>'0'),$fields[0]);
	$fieldList = array_flip($fieldList);

	$fname = variable_get('anatoa_FirstName',0);
	$lname = variable_get('anatoa_LastName',0);
	$country = variable_get('anatoa_Country',0);

	$defaultValues['First Name'] = ($fname != 0) ? $fname : $defaultValues['First Name'];
	$defaultValues['Last Name'] = ($lname != 0) ? $lname : $defaultValues['Last Name'];
	$defaultValues['Country'] = ($country != 0) ? $country : $defaultValues['Country'];

	$form['#cache'] = FALSE;

	$form['FieldMap'] = array(
		'#title' => t('Field Mapping'),
		'#type' => 'fieldset'
		);

	$form['FieldMap']['UserName'] = array(
		'#title' => t('User Name'),
		'#type' => 'item',
		'#description' => "`name` ".t("field from")." table `users`.",
		);
	$form['FieldMap']['Email'] = array(
		'#title' => t('Email'),
		'#type' => 'item',
		'#description' => "`mail` ".t("field from")." table `users`.",
		);
	$form['FieldMap']['FirstName'] = array(
		'#title' => t('First Name'),
		'#type' => 'select',
		'#default_value'=>$defaultValues['First Name'],
		'#description' => t(" field from")." table `profile_fields`.",
		'#options' =>  $fieldList
		);
	$form['FieldMap']['LastName'] = array(
		'#title' => t('Last Name'),
		'#type' => 'select',
		'#default_value'=> $defaultValues['Last Name'],
		'#description' => t(" field from")." table `profile_fields`.",
		'#options' => $fieldList
		);

	$anatoaConfig = anatoaConfiguration::getInstance();

	$form['FieldMap']['Country'] = $anatoaConfig->reqMod['anatoa_addresses_user']
								? array('#title' => t('Country'),
										'#type' => 'item',
										'#default_value'=> 0,
										'#description' => "Country ".t(" field from table `addresses_user`."))

								: array(
									'#title' => t('Country'),
									'#type' => 'select',
									'#default_value'=> $defaultValues['Country'],
									'#description' => t(" field from")." table `profile_fields`.",
									'#options' => $fieldList
									);

	$form['FieldMap']['IPAddress'] = array(
		'#title' => t('IP Address'),
		'#type' => 'item',
		'#description' => "IP Address ".t(" shall be auto detected while user logs in."),
		);
	$form['FieldMap']['ProxyIP'] = array(
		'#title' => t('Proxy IP Address'),
		'#type' => 'item',
		'#description' => "Proxy IP Address ".t(" shall be auto detected while user logs in."),
		);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
		);
	$form['cancel'] = array(
		'#type' => 'button',
		'#value' => t('Cancel')
		);

	return $form;
}

function anatoa_mapfieldsForm_submit($form, &$form_state)
{
	variable_set('anatoa_FirstName',$form_state['values']['FirstName']);
	variable_set('anatoa_LastName',$form_state['values']['LastName']);
	variable_set('anatoa_Country',$form_state['values']['Country']);
	drupal_set_message(t('Anatoa plugin fields mapped successfully'));
}

function anatoa_listProfileLog()
{
	$anatoaConfig = anatoaConfiguration::getInstance();

	$output = '<br>';
	$header = array(
	array('data' => t('Date'), 'field' => 'LogDate', 'sort' => 'desc'),
	array('data' => t('Member ID'), 'field' => 'MemberId'),
	array('data' => t('User Name'), 'field' => 'UserName'),
	array('data' => t('Result'), 'field'=>'Status'));

	$result = pager_query('SELECT LogDate, MemberId, users.name UserName, anatoa_users.name AS OrigName, Master.Status, Master.CheckStatus
								FROM anatoa_transactionlog AS log
    							LEFT JOIN users ON users.uid = log.MemberId
       							LEFT JOIN anatoa_users ON anatoa_users.uid = log.MemberId
       							LEFT JOIN anatoa_mst_profiles AS Master ON log.RecordID = Master.ProfileID
       								AND	log.TableName = Master.TableName AND Master.ProfileID=log.MemberId
       				WHERE TransactionType = \'Check Profile\''. tablesort_sql($header), 30, 0, NULL);

	$rows = array();
	while ($log = db_fetch_object($result)) {

		$userName = empty($log->UserName) ? l($log->OrigName,'admin/user/anatoa/viewDeletedProfile/'.$log->MemberId) :   l($log->UserName,'user/'.$log->MemberId);

		switch($log->Status)
		{
			case 'Unconfirmed': $status = ($log->CheckStatus == 'Checked') ? 'Pass' : 'Failed Check' ; break;
			case 'Deleted': $status = 'Possible Fraud - Deleted' ; break;
			case 'Block': $status = 'Possible Fraud - Blocked' ; break;
		}

		$rows[] = array(
		array('data' => date('Y-m-d H:i:s',$log->LogDate), 'class' => 'nowrap'),
		'<div align="center">'.$log->MemberId.'</div>',
		$userName,
		t($status));
	}

	if (empty($rows)) {
		$rows[] = array(array('data' => t('No Log available.'), 'colspan' => 4));
	}

	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, 30, 0);
	return $output;

}

function anatoa_listImageLog()
{
	$anatoaConfig = anatoaConfiguration::getInstance();

	$output = '<br>';
	$header = array(
	array('data' => t('Date'), 'field' => 'LogDate', 'sort' => 'desc'),
	array('data' => t('User Name'), 'field' => 'UserName'),
	array('data' => t('Image'), 'field' => 'ImageFile'),
	array('data' => t('Result'), 'field'=>'Status'));

	$result = pager_query('SELECT LogDate,MemberId,User.name AS UserName,anatoa_users.name OrigName,ImageFile,MasterImage.Status,MasterImage.CheckStatus
							FROM {anatoa_transactionlog} AS log
    							LEFT JOIN {anatoa_mst_images} AS MasterImage ON log.RecordID = MasterImage.ImageID AND MasterImage.TableName = log.TableName
    							LEFT JOIN users AS User ON User.uid = log.MemberId
    							LEFT JOIN anatoa_users ON anatoa_users.uid = log.MemberId
    						WHERE TransactionType = \'Check Image\''. tablesort_sql($header), 30, 0, NULL);
	$rows = array();
	while ($log = db_fetch_object($result)) {

		$userName = empty($log->UserName) ? l($log->OrigName,'user/'.$log->MemberId) : l($log->UserName,'admin/user/anatoa/viewDeletedProfile/'.$log->MemberId);

		switch($log->Status)
		{
			case 'Unconfirmed': $status = ($log->CheckStatus == 'Checked') ? 'Pass' : 'Failed Check' ; break;
			case 'Deleted': $status = 'Possible Fraud - Deleted' ; break;
			case 'Block': $status = 'Possible Fraud - Blocked' ; break;
		}

		$rows[] = array(
		array('data' => date('Y-m-d H:i:s',$log->LogDate), 'class' => 'nowrap'),
		$userName,
		'<img src=\''.$log->ImageFile.'\' height=100 width=100 />',
		t($status));
	}

	if (empty($rows)) {
		$rows[] = array(array('data' => t('No Log available.'), 'colspan' => 4));
	}

	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, 30, 0);
	return $output;
}

function anatoa_listMsgLog()
{
	$anatoaConfig = anatoaConfiguration::getInstance();
	$output = '<br>';
	$header = array(
	array('data' => t('Date'), 'field' => 'LogDate', 'sort' => 'desc'),
	array('data' => t('User Name'), 'field' => 'UserName'),
	array('data' => t('Message'), 'field' => 'TableName'),
	array('data' => t('Result'), 'field'=>'Status'));

	$result = pager_query('SELECT LogDate,MemberId,User.name AS UserName,anatoa_users.name OrigName,log.TableName,MasterMsg.Status,log.parameters,MasterMsg.CheckStatus
								FROM {anatoa_transactionlog} AS log
    							LEFT JOIN {anatoa_mst_messages} AS MasterMsg ON log.RecordID = MasterMsg.MessageID AND MasterMsg.TableName = log.TableName
    							LEFT JOIN users AS User ON User.uid = log.MemberId
    							LEFT JOIN anatoa_users ON anatoa_users.uid = log.MemberId
    						WHERE TransactionType = \'Check Message\''. tablesort_sql($header), 30, 0, NULL);
	$rows = array();
	while ($log = db_fetch_object($result)) {

		$userName = empty($log->UserName) ? l($log->OrigName,'user/'.$log->MemberId) : l($log->UserName,'admin/user/anatoa/viewDeletedProfile/'.$log->MemberId);

		switch($log->Status)
		{
			case 'Unconfirmed': $status = ($log->CheckStatus == 'Checked') ? 'Pass' : 'Failed Check' ; break;
			case 'Deleted': $status = 'Possible Fraud - Deleted' ; break;
			case 'Block': $status = 'Possible Fraud - Blocked' ; break;
		}

		preg_match_all('/"([^\"]+)\"/',$log->parameters , $matches, PREG_OFFSET_CAPTURE);
		$rows[] = array(
		array('data' => date('Y-m-d H:i:s',$log->LogDate), 'class' => 'nowrap'),
		$userName,
		$matches[1][1][0],
		t($status));
	}

	if (empty($rows)) {
		$rows[] = array(array('data' => t('No Log available.'), 'colspan' => 4));
	}

	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, 30, 0);
	return $output;
}

function anatoa_deletedProfileArchive(&$form_state = NULL)
{
	$anatoaConfig = anatoaConfiguration::getInstance();
	$status = '';

	$form['#prefix'] = '<br>'.t('The Anatoa fraud detection plugin has automatically deleted the following profiles from your member database based on your configuration settings. Use this page to review profiles and optionally restore any \'false positives\' (profiles which you believe are legitimate and would like to allow on your system).').'<br><br>';
	$form['header'] = array(
					'#type' => 'value',
					'#value' => array(
								    theme('table_select_header_cell'),
								    array('data' => t('User Name'), 'field' => 'name'),
									array('data' => t('Email'), 'field' => 'mail'),
									array('data' => t('Registration Date'), 'field' => 'created'),
									array('data' => t('Last Login'), 'field' => 'login','sort' => 'desc'),
									array('data' => t('Status'), 'field' => 'status'),
  									 ));

  $result = pager_query("SELECT uid,name,mail,created,login FROM {anatoa_users} AS anataoProfile
    							LEFT JOIN {anatoa_mst_profiles} AS Master ON anataoProfile.uid = Master.ProfileID
    						WHERE  Master.Status = 'Delete' AND
    							  (Master.reportDate < Master.checkDate OR
    							  			((Master.reportReasons = 'Message found to be fraudulent'
    							  				OR Master.reportReasons = 'Image found to be fraudulent'
    							  				OR Master.reportReasons = 'Profile found to be fraudulent')
    							  			  AND Master.reportDate > Master.checkDate)) ORDER BY reportDate DESC,checkDate DESC", 10, 0, NULL);

  while ($user = db_fetch_object($result))
  {
    $users[$user->uid] = '';
    $status = $user->status ? 'Active' : 'Blocked' ;
    $form['name'][$user->uid] = array('#value' => l($user->name,'admin/user/anatoa/viewDeletedProfile/'.$user->uid));
	// theme('username', $user));
    $form['mail'][$user->uid] = array('#value' => $user->mail);
    $form['created'][$user->uid] = array('#value' => format_date($user->created, 'small'));
    $form['login'][$user->uid] = array('#value' => format_date($user->created, 'small'));
    $form['status'][$user->uid] = array('#value' => $status);
  }

  $form['users'] = array('#type' => 'checkboxes', '#options' => isset($users) ? $users: array());
  $form['pager'] = array('#value' => theme('pager', NULL, 10, 0));

  if(!empty($status))
  {
  	$form['restoreBlocked'] = array(
  		'#name' => 'restore',
		'#type' => 'submit',
		'#value' => t('Restore Selected Profiles')
		);
  }

  $form['#theme'] = 'anatoa_userList';

  return $form;

}

function anatoa_deletedProfileArchive_validate($form, &$form_state)
{
  $form_state['values']['users'] = array_diff($form_state['values']['users'], array(0));

  if (count($form_state['values']['users']) == 0)
    form_set_error('', t('Please select one or more user to perform the action.'));
}

function anatoa_deletedProfileArchive_submit($form, &$form_state)
{
	foreach ($form_state['values']['users'] as $uid => $value)
	{
		$objDBModel = new dbModel();
		$objDBModel->restoreMember($value);
    }

    drupal_set_message(t('Selected profile(s) have been restored.'));
}

function anatoa_reportProfileForm(&$form_state = NULL)
{
	$form['#prefix'] =  '<br>'.t('Use this page to review profiles and report profile(s) which are fradulent.').' <br><br>';
	$form['header'] = array(
					'#type' => 'value',
					'#value' => array(
								    theme('table_select_header_cell'),
								    array('data' => t('User Name'), 'field' => 'name'),
									array('data' => t('Email'), 'field' => 'mail'),
									array('data' => t('Registration Date'), 'field' => 'created'),
									array('data' => t('Last Login'), 'field' => 'login','sort' => 'desc'),
									array('data' => t('Status'), 'field' => 'status'),
  									 ));

  $result = pager_query('SELECT uid,name,mail,created,login,status FROM {users} WHERE uid > 1', 10, 0, NULL);

  while ($user = db_fetch_object($result))
  {
    $users[$user->uid] = '';
    $status = $user->status ? 'Active' : 'Blocked' ;
    $form['name'][$user->uid] = array('#value' => theme('username', $user));
    $form['mail'][$user->uid] = array('#value' => $user->mail);
    $form['created'][$user->uid] = array('#value' => format_date($user->created, 'small'));
    $form['login'][$user->uid] = array('#value' => format_date($user->login, 'small'));
    $form['status'][$user->uid] = array('#value' => $status);
  }

  $form['users'] = array('#type' => 'checkboxes', '#options' => isset($users) ? $users: array());
  $form['pager'] = array('#value' => theme('pager', NULL, 10, 0));

  $form['reportNBlock'] = array(
  		'#name' => 'reportNBlock',
		'#type' => 'submit',
		'#value' => t('Report and block')
		);
  /*$form['reportNDelete'] = array(
  		'#name' => 'reportNDelete',
		'#type' => 'submit',
		'#value' => t('Report and delete')
		);*/

  $form['#theme'] = 'anatoa_reportUserList';
  return $form;
}

function anatoa_reportProfileForm_validate($form, &$form_state)
{
  $form_state['values']['users'] = array_diff($form_state['values']['users'], array(0));

  if (count($form_state['values']['users']) == 0)
    form_set_error('', t('Please select one or more user to perform the action.'));
}

function anatoa_reportProfileForm_submit($form, &$form_state)
{
	$objwebService = new anatoaWebService();
	$j=0;
	$resonsOther = 0;
	$mode = 'Block';  //($_POST['reportNDelete']) ? 'Delete' :

	foreach ($form_state['values']['users'] as $uid => $value)
	{
		if($_POST['myoption'][$j] != 'Other')
		{
			$return = $objwebService->reportProfile($uid,$_POST['myoption'][$j],$mode);
		}
		else
		{
			$return = $objwebService->reportProfile($uid,"Other : ".$_POST['text'][$resonsOther],$mode);
			$resonsOther++;
		}

		if($return == 'Error:Connection' || $return == 'Error:APIKey') break;
		$j++;
    }

    if(empty($return))
    	drupal_set_message(t('Profile(s) reported to Anatoa.com. Thank you for your contribution.'));
    elseif($return == 'Error:Connection' || $return == 'Error:APIKey')
		drupal_set_message(t('Could not report profile due to error in connecting web server via SOAP call.'));
	else drupal_set_message(t("Unexpected error occurred while reporting profile. <br> Server response : $return"));

}

function anatoa_checkOldProfiles()
{
	$objDBModel = new dbModel();
	$numProfilesUnchecked = $objDBModel->getNumberProfileUnchecked(0);
	if($numProfilesUnchecked > 0)
	{
		$numImagesUnchecked = $objDBModel->getNumberImageUnchecked();
		$numMessagesUnchecked = $objDBModel->getNumberMessageUnchecked();
	}
	else $numImagesUnchecked = $numMessagesUnchecked = 0;

	drupal_add_js(drupal_get_path('module', 'anatoa') .'/js/anatoa.js');

	$output = '<h3>'.t('Check Old Profiles').'</h3>';
	$output .= '<div style="text-align:justify">'.t("Your database contains $numProfilesUnchecked profiles, $numImagesUnchecked images and $numMessagesUnchecked messages which have not been checked by Anatoa. You can use this page to check this data against our fraud detection service. Please be aware that the checks you run from this page will count against your monthly quota, which depends on your subscription level. If you have a large amount of data to check, we suggest you (temporarily) increase your subscription level, or spread usage of this page over several months.").'</div><br/>';

	$numProfilesUncheckedwithIPaddr = $objDBModel->getNumberProfileUnchecked(0,'Recheck');
	$profDiff = $numProfilesUncheckedwithIPaddr-$numProfilesUnchecked;
	if($profDiff > 0)
		$output .= "<div >".t('The Anatoa plugin has collected user IP address for $profDiff of your previously checked old profiles. It is recommended that you recheck these profiles with this additional information.')." </div>";

	$output .= "<div id='anatoaMsg' class='status'></div><br/>";
	$output .= drupal_get_form('anatoa_checkOldProfilesForm');

	return $output;
}

function anatoa_checkOldProfilesForm(&$form_state )
{
	$anatoaConfig = anatoaConfiguration::getInstance();
	$numOfProfiles='';
	$numOfProfileToCheck ='';
	$recheckValue=true;

	if (isset($form_state['values'])) {

		$objDBModel = new dbModel();
		$numOfProfiles = $form_state['values']['numProfiles'];

		$recheckValue = $form_state['values']['Recheck'];
		if($numOfProfiles > 0)
		{
			$styleVisibility =  'block';
			$recheck = $form_state['values']['Recheck'] ? 'Recheck' : '';
			$numOfProfileToCheck = $objDBModel->getNumberProfileUnchecked($numOfProfiles,$recheck);
		}
		else $styleVisibility =  'none';

		if(strpos($objDBModel->strProfileIds,',') === false)
  			$objDBModel->strProfileIds = "'$objDBModel->strProfileIds'";
    		$totalProfiles = count($objDBModel->profile_ids);

		drupal_add_js('
			var APIKeyNotConfigured = "'.t($anatoaConfig->genMsg['APIKeyNotConfigured']).'";
			var ConnectionError = "'.t($anatoaConfig->genMsg['ConnectionError']).'";

			var profileIDs = new Array('.$objDBModel->strProfileIds.');
			var totalItems = '.$totalProfiles.";
			var ProfileToScan = $totalProfiles;
			var chkProfileUrl = '".url('admin/user/anatoa/checkProfile')."';

			var error = 0;
			var debugMsg ='';
			var fProfile = 0;
			var ProfilesScanned = 0;
			var runStatus = 0;
			var temp = new Array();
			var scanned = new Array();
			var foundFraud = new Array();
			var percentageCompleted=1;
			var i=0;",'inline','footer',FALSE,FALSE);

		if($totalProfiles > 0)
			$styleVisibility =  'block';
		else if($form_state['values']['numProfiles'] != $totalProfiles && $form_state['values']['numProfiles'] != 0)
		{
			$form['#prefix'] = '<div class=\'status error\'>'.t($anatoaConfig->genMsg['AllProfileCheck']).'</div>';
			$styleVisibility =  'none';
		}

	}
	else $styleVisibility = 'none';

	$form['CheckOldProfile'] = array(
			'#title' => '',
			'#type' => 'fieldset'
			);

	$form['CheckOldProfile']['Recheck'] = array(
			'#title' => t('Recheck profiles with collected IP address'),
			'#type' => 'checkbox',
			'#default_value' => $recheckValue,
			'#description' => '',
			);
	$form['CheckOldProfile']['numProfiles'] = array(
			'#title' => t('Maximum number of profiles to be checked'),
			'#type' => 'textfield',
			'#required' => TRUE,
			'#default_value' => $numOfProfiles,
			'#description' => "",
			);
	$form['CheckOldProfile']['selectProfile'] = array(
			'#type' => 'submit',
			'#id' => 'startbgProcbutton',
			'#value' => t('Prepare for background processing')
			);

	$form['CheckOldProfile2'] = array(
			'#title' => '',
			'#prefix' => "<div id='hiddenFieldset' style='display:$styleVisibility'>",
			'#type' => 'fieldset',
			'#suffix' => '</div>',
			'#visible' => false
			);

	$form['CheckOldProfile2']['divElement'] = array(
			'#type' => 'item',
			'#value' => "<table border='0' width='90%' style='border-collapse: separate;' >
								<tr>
									<td width='50%' align='center'>
										<table border='0' style='border-collapse: separate;'>
											<tr>
											<td>".t('Total number of Profiles to Check')." :</td>
											<td><b> $numOfProfileToCheck </b></td>
											</tr>
										</table>
									</td>
									<td bgcolor='#333333' style='width:1px; padding:1px;' > </td>
									<td width='50%' align='center'>
										<table border='0' style='border-collapse: separate;'>
											<tr>
												<td>".t('Number of Fraudulent Profiles found')." :</td>
												<td><b style='color:red; '><div id='fP'> 0 </div></b></td>
											</tr>
										</table>
									</td>
								</tr>
							</table><br>
			<div align='center'>
				<div style='width: 80%; height: 30px; background-color: #CCCCCC' align='left'>
					<div id='divPercent' style='width: 0.5%; height: 30px; background-color: #009966; text-align: center; vertical-align: middle;'>
					<div id='percentageStatus' align='center'	style='vertical-align: middle;'></div>
				</div>
			</div>
			<div style='width: 70%;' align='left' id='noOfScanned'>
				(<span	id='fPComplete'>0</span> ".t('of')." $numOfProfileToCheck) ".t('profiles scanned')."
			</div>
			<div style='width: 70%;' align='center'><br>
					<input type='button' name='startButton' id='startButton' value='".t('Start')."' style='width: 100px' > &nbsp;&nbsp; 	<input	type='button' name='pauseButton' id='pauseButton' value='".t('Pause')."' style='width: 100px' disabled> &nbsp;&nbsp;
			</div>",
			);

	return $form;
}

function anatoa_checkOldProfilesForm_validate($form, &$form_state) {
	if (( $form_state['values']['numProfiles'] === 0 || $form_state['values']['numProfiles'] < 1) && $form_state['values']['numProfiles'] != '')
	{
		// We notify the form API that this field has failed validation.
		form_set_error('APIKey',t('Please enter a valid number of profiles.'));
	}

	$form_state['rebuild'] = TRUE;

}

function anatoa_checkOldProfilesForm_submit($form, &$form_state) {
}

function anatoa_checkProfile()
{
   if (!empty($_POST['profileId']))
   {
		// jQuery made the call.
		// This will return results to jQuery's request.

		$objwebService = new anatoaWebService();
		$return = $objwebService->ajaxCheckProfile((int)$_POST['profileId']);
		drupal_json(array('responseText' => $return));

		exit();
	}
}

function anatoa_viewProfile($profileId)
{
	global $base_url;
	$anatoaConfig = anatoaConfiguration::getInstance();
	$objDBModel = new dbModel();
	$memberData =  $objDBModel->getArchiveMemberData($profileId);
	$fields = $objDBModel->getFieldList();

	$firstName = '';
	$lastName = '';
	$country = '';
	$firstNameFieldID = variable_get('anatoa_FirstName',0);
	if($firstNameFieldID != 0)
		$firstNameField = $fields[2][$firstNameFieldID];

	$lastNameFieldID = variable_get('anatoa_LastName',0);
	if($lastNameFieldID != 0)
		$lastNameField = $fields[2][$lastNameFieldID];

	$countryFieldID = variable_get('anatoa_Country',0);
	if($countryFieldID != 0)
		$countryField = $fields[2][$countryFieldID];

	$customFieldsData = $objDBModel->getCustomProfileFieldData($profileId, $firstNameFieldID.','. $lastNameFieldID. ','. $countryFieldID);

	$reasonForDel = '';
   switch ($memberData['reportReasons'])
		     {
		     	case 'Message found to be fraudulent':
					$reasonForDel = $anatoaConfig->reasonForProfileDeletion['Message']; break;
		     	case 'Image found to be fraudulent':
					$reasonForDel = $anatoaConfig->reasonForProfileDeletion['Image']; break;
		     	default: $reasonForDel = $anatoaConfig->reasonForProfileDeletion['Profile'];
		     }

	$output = t('<h2> Profile Detail </h2>');
	$header = array(t('Field Name'), t('Value'));
	$rows[] = array( 'User ID' , $memberData['uid']);
	$rows[] = array( 'User Name' , $memberData['name']);
	$rows[] = array( 'Email' , $memberData['mail']);
	$rows[] = array( 'FirstName' , $customFieldsData[$firstNameFieldID]);
	$rows[] = array( 'Last Name' , $customFieldsData[$lastNameFieldID]);
	$rows[] = array( 'Country' , $customFieldsData[$countryFieldID]);
	$rows[] = array( 'IP Address' , $memberData['IPAddress']);
	$rows[] = array( 'Proxy IP' , $memberData['ProxyIP']);
	$rows[] = array( 'Registration Date' , format_date($memberData['created'], 'small'));
	$rows[] = array( 'Last Login' , format_date($memberData['login'], 'small'));
	$rows[] = array( 'Reason for deletion' , $reasonForDel);

	$output .= theme('table',$header, $rows);
	$output .= '<br>';


	$messageData =  $objDBModel->getMessagesOfDeletedProfile($profileId);
	if(!empty($messageData))
	{
		$rows=array();
		$header = array(t('Table Name'), t('Subject'),t('Message'),t('Date'));
		foreach($messageData as $msg)
			$rows[] = array($msg['tableName'],$msg['Subject'],$msg['Message'],format_date($msg['Date'], 'small'));

		$output .= t('<h2> Profile Messages </h2>');
		$output .= theme('table',$header, $rows);
		$output .= '<br>';
	}

	$imageData =  $objDBModel->getImagesOfDeletedProfile($profileId);
	if(!empty($imageData))
	{
		$rows=array();
		$header = array(t('Table Name'), t('Title'),t('File'),t('Date'));
		foreach($imageData as $img)
		{
			if(!empty($img['Filename']))
				$rows[] = array($img['tableName'],$img['Title'],'<img src=\''.$base_url.'/'.$img['Filename'].'\' width = 110, height=100 />',format_date($img['DateCreated'], 'small'));
		}

		if(!empty($rows))
		{
			$output .= t('<h2> Profile Images </h2>');
			$output .= theme('table',$header, $rows);
			$output .= '<br>';
		}
	}

	$output .= '<br> <input type=button value="'.t('Back').'" onClick="window.history.go(-1)"';

	return $output;
}
